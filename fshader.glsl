#version 450 

in vec4 shadedColor;
in vec2 texCoord;

/* Phong shading attributes*/
in vec3 fragNormal;
in vec3 fragPosition;

out vec4 color;

struct Material {
    bool diffuseActive;
    sampler2D diffuse;

    bool specularActive;
    sampler2D specular;

    float shininess;
};


struct DirectionalLight {
    bool enabled;

    vec4 ambient;
    vec4 diffuse;
    vec4 specular;

    vec3 direction;
};

struct PointLight {
    bool enabled;

    // attenuation terms
    float constant;
    float linear;
    float quadratic;

    vec4 ambient;
    vec4 diffuse;
    vec4 specular;

    vec3 position;
};


struct SpotLight {
    bool enabled;

    // attenuation terms
    float constant;
    float linear;
    float quadratic;

    vec4 ambient;
    vec4 diffuse;
    vec4 specular;

    vec3 position;

    // spotlight attributes
    vec3 direction;

    float cutOffCosine;
    float outerCutOffCosine;
};


uniform uint DrawMode; // Draw mode 0 for wireframe, 1 for shading, 2 for texture, 3 for both texture and shading
uniform uint ReflectionModel; // Reflection mode 0 for phong, 1 for modified phong

uniform vec3 cameraPosition; // position of the camera

uniform Material material;


// Light related uniforms
const int MAX_DIRECTIONAL_LIGHT_SOURCES = 5;
const int MAX_POINT_LIGHT_SOURCES = 25;
const int MAX_SPOT_LIGHT_SOURCES = 5;

// Source arrays
uniform DirectionalLight directionalLightSources[MAX_DIRECTIONAL_LIGHT_SOURCES];
uniform PointLight pointLightSources[MAX_POINT_LIGHT_SOURCES];
uniform SpotLight spotLightSources[MAX_SPOT_LIGHT_SOURCES];

// Used sources (preventing iteration on whole array.)
uniform int lastDirectionalSource;
uniform int lastPointSource;
uniform int lastSpotSource;

// Function prototypes
vec4 calculateDirectionalLightParticipation(DirectionalLight light, vec3 normal, vec3 viewDirection);
vec4 calculatePointLightParticipation(PointLight light, vec3 normal, vec3 viewDirection);
vec4 calculateSpotLightParticipation(SpotLight light, vec3 normal, vec3 viewDirection);


void main(){
    if(DrawMode == 1 || DrawMode == 3){ // phong shading
        vec3 N = normalize(fragNormal);
        vec3 E = normalize(cameraPosition - fragPosition);

        // Loop adds up color contributions for each light source
        int i;
        color = vec4(0.0, 0.0, 0.0, 0.0);

        int pointSourceIterationCount = min(lastPointSource, MAX_POINT_LIGHT_SOURCES - 1);
        int directionalSourceIterationCount = min(lastDirectionalSource, MAX_DIRECTIONAL_LIGHT_SOURCES - 1);
        int spotSourceIterationCount = min(lastSpotSource, MAX_SPOT_LIGHT_SOURCES);

        for(i = 0; i <= pointSourceIterationCount; i++){
            if (pointLightSources[i].enabled){
                color += calculatePointLightParticipation(pointLightSources[i], N, E);
            }
        }

        for(i = 0; i <= directionalSourceIterationCount; i++){
            if (directionalLightSources[i].enabled){
                color += calculateDirectionalLightParticipation(directionalLightSources[i], N, E);
            }
        }

        for(i = 0; i <= spotSourceIterationCount; i++){
            if (spotLightSources[i].enabled){
                color += calculateSpotLightParticipation(spotLightSources[i], N, E);
            }
        }

    } else if(DrawMode == 2 && material.diffuseActive){ // Texture mode
        color = texture( material.diffuse, texCoord );
    } else { // Color already calculated at vertex. Wireframe
        color = shadedColor;
    }
}


vec4 calculateDirectionalLightParticipation(DirectionalLight light, vec3 normal, vec3 viewDirection){
    vec3 lightDirection = normalize(-light.direction);

    // Default initialization.
    vec4 ambient = vec4(0.0, 0.0, 0.0, 0.0);
    vec4 diffuse = vec4(0.0, 0.0, 0.0, 0.0);
    vec4 specular = vec4(0.0, 0.0, 0.0, 0.0);

    // Calculate K values.

    // diffuse
    float difuseK = max( dot(lightDirection, normal), 0.0 );

    // specular;
    float specularK = 0.0;
    if (dot(lightDirection, normal) < 0.0){
        if (ReflectionModel == 1){ // modified phong.
            vec3 H = normalize( lightDirection + viewDirection ); // halfway vector
            specularK = pow( max(dot(normal, H), 0.0), material.shininess );
        } else {
            vec3 R = normalize(2*(dot(lightDirection, normal)*normal) - lightDirection);
            specularK = pow( max(dot(R, viewDirection), 0.0), material.shininess );
        }
    }

    // Calculate color contributions and combine results
    ambient = light.ambient * texture(material.diffuse, texCoord);

    if(DrawMode == 3 && material.diffuseActive) // Must be texture draw mode and have diffuse map.
        diffuse = difuseK * light.diffuse * texture( material.diffuse, texCoord );
    else
        diffuse = difuseK * light.diffuse * shadedColor;

    if(material.specularActive){ // Must have a specular map.
        specular = specularK * light.specular * texture( material.specular, texCoord );
    } // No specular component if there is no specular map.

    return ambient + diffuse + specular;
}



vec4 calculatePointLightParticipation(PointLight light, vec3 normal, vec3 viewDirection){
    vec3 lightDirection = normalize(light.position - fragPosition);

    // Default initialization.
    vec4 ambient = vec4(0.0, 0.0, 0.0, 0.0);
    vec4 diffuse = vec4(0.0, 0.0, 0.0, 0.0);
    vec4 specular = vec4(0.0, 0.0, 0.0, 0.0);

    // diffuse
    float diffuseK = max( dot(lightDirection, normal), 0.0 );

    // specular
    float specularK = 0.0;
    if (dot(lightDirection, normal) < 0.0){
        if (ReflectionModel == 1){ // modified phong.
            vec3 H = normalize( lightDirection + viewDirection ); // halfway vector
            specularK = pow( max(dot(normal, H), 0.0), material.shininess );
        } else {
            vec3 R = normalize(2*(dot(lightDirection, normal)*normal) - lightDirection);
            specularK = pow( max(dot(R, viewDirection), 0.0), material.shininess );
        }
    }

    // Attenuation
    float dist = distance(light.position, fragPosition);
    float attenuation = 1.0 / (light.constant + light.linear * dist + light.quadratic * dist * dist);

    // Calculate color contributions and combine results
    ambient = light.ambient * texture(material.diffuse, texCoord);

    if(DrawMode == 3 && material.diffuseActive) // Must be texture draw mode and have diffuse map.
        diffuse = diffuseK * light.diffuse * texture( material.diffuse, texCoord );
    else
        diffuse = diffuseK * light.diffuse * shadedColor;

    if(material.specularActive){ // Must have a specular map.
        specular = specularK * light.specular * texture( material.specular, texCoord );
    } // No specular component if there is no specular map.

    return (ambient + diffuse + specular) * attenuation;
}


vec4 calculateSpotLightParticipation(SpotLight light, vec3 normal, vec3 viewDirection){
    vec3 lightDirection = normalize(light.position - fragPosition);

    // Default initialization.
    vec4 ambient = vec4(0.0, 0.0, 0.0, 0.0);
    vec4 diffuse = vec4(0.0, 0.0, 0.0, 0.0);
    vec4 specular = vec4(0.0, 0.0, 0.0, 0.0);

    // diffuse
    float diffuseK = max( dot(lightDirection, normal), 0.0 );

    // specular
    float specularK = 0.0;
    if (dot(lightDirection, normal) < 0.0){
        if (ReflectionModel == 1){ // modified phong.
            vec3 H = normalize( lightDirection + viewDirection ); // halfway vector
            specularK = pow( max(dot(normal, H), 0.0), material.shininess );
        } else {
            vec3 R = normalize(2*(dot(lightDirection, normal)*normal) - lightDirection);
            specularK = pow( max(dot(R, viewDirection), 0.0), material.shininess );
        }
    }

    // Attenuation
    float dist = distance(light.position, fragPosition);
    float attenuation = 1.0 / (light.constant + light.linear * dist + light.quadratic * dist * dist);

    // Soft Spot light effect
    float delta = dot(lightDirection, normalize(-light.direction));
    float epsilon = (light.cutOffCosine - light.outerCutOffCosine);
    float spotIntensity = clamp((delta - light.outerCutOffCosine) / epsilon, 0.0, 1.0); // iterpolation

    // Calculate color contributions and combine results
    ambient = light.ambient * texture(material.diffuse, texCoord);

    if(DrawMode == 3 && material.diffuseActive) // Must be texture draw mode and have diffuse map.
        diffuse = spotIntensity * diffuseK * light.diffuse * texture( material.diffuse, texCoord );
    else
        diffuse = spotIntensity * diffuseK * light.diffuse * shadedColor;

    if(material.specularActive){ // Must have a specular map.
        specular = spotIntensity * specularK * light.specular * texture( material.specular, texCoord );
    } // No specular component if there is no specular map.

    return (ambient + diffuse + specular) * attenuation;
}