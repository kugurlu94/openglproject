#ifndef __TEXTURE_H
#define __TEXTURE_H

#include <vector>
#include <Angel.h>
#include <SOIL.h>
#include "utils.h"

/* Holds relevant data for 2D texture image */
class Texture {
    protected:
        // texture image size
        GLsizei width;
        GLsizei height;
        GLuint tex_id;

        // image data.
        const GLubyte * image;

        bool allocated;

    public:
        explicit Texture(GLsizei width, GLsizei height, const GLubyte * image);
        virtual ~Texture();

        /* Allocation in gpu */
        void allocate();
        void deallocate();
        bool isAllocated();
        void use();
        static void unUse();

        /* update texture properties */
        virtual void updateProperties();

        static Texture * fromPPM(string imageFilename);
        static Texture * fromImageFile(string imageFilename);
};


class TextureManager {
    private:
        vector<Texture *> textures;

        static TextureManager * instance;
    public:
        // Image constructors
        unsigned int newTexture(GLsizei width, GLsizei height, const GLubyte *image);
        unsigned int fromPPM(string imageFilename);
        unsigned int fromImageFile(string imageFilename);

        // Method for removing all textures.
        void removeAll();

        // Method for obtaining textures.
        Texture * getTexture(unsigned int tex_id) const;

        // Method for obtaining instance.
        static TextureManager * getInstance();
};

#endif