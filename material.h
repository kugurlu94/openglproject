#ifndef __MATERIAL_H
#define __MATERIAL_H

#include <Angel.h>

typedef Angel::vec4 color4;

class MaterialProperties {
    protected:
        /* Default reflectivity components */
        static const color4 DEFAULT_EMISSION;
        static const GLfloat DEFAULT_SHININESS;

        /* Light components members */
        unsigned int diffuse;
        unsigned int specular;

        /* Emission */
        color4 emission;

        /* Shininess */
        GLfloat shininess;

    public:
        /* Explicit constructor */
        explicit MaterialProperties(
            const unsigned int& diffuse = 0,
            const unsigned int& specular = 0,
            const color4& emission = DEFAULT_EMISSION,
            const GLfloat& shininess = DEFAULT_SHININESS
        );

        /* Getters */
        unsigned int getDiffuse();
        unsigned int getSpecular();
        color4 getEmission();
        GLfloat getShininess();

        /* Setters */
        void setDiffuse(const unsigned int& diffuse);
        void setSpecular(const unsigned int& specular);
        void setEmission(const color4& emission);
        void setShininess(const GLfloat& mat_shininess);
};

#endif