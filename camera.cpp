#include "camera.h"

const GLfloat Camera::DEFAULT_YAW = -90.0;
const GLfloat Camera::DEFAULT_PITCH = 0.0;
const GLfloat Camera::MAX_PITCH = 89.0;
const GLfloat Camera::MIN_PITCH = -89.0;


Camera::Camera(const point4& position, const GLfloat& yaw, const GLfloat& pitch)
    : position(position),
      yaw(yaw),
      pitch(pitch),
      up(vec4(0.0, 1.0, 0.0, 0.0)),
      viewFresh(false),
      vectorsFresh(false){}

Camera::Camera(const GLfloat& position_x, const GLfloat& position_y, const GLfloat& position_z, const GLfloat& yaw, const GLfloat& pitch)
    : Camera::Camera(point4(position_x, position_y, position_z, 1.0), yaw, pitch){}

Camera::Camera()
    : Camera::Camera(point4(0.0, 0.0, 0.0, 1.0)){}


void Camera::moveX(const GLfloat& delta_x){
    this->position.x = this->position.x + delta_x;
    this->viewFresh = false;
}

void Camera::moveY(const GLfloat& delta_y){
    this->position.y = this->position.y + delta_y;
    this->viewFresh = false;
}

void Camera::moveZ(const GLfloat& delta_z){
    this->position.z = this->position.z + delta_z;
    this->viewFresh = false;
}

void Camera::move(const vec3& move_vec){
    this->position = this->position + vec4(move_vec, 0.0);
    this->viewFresh = false;
}

void Camera::pitchCamera(const GLfloat& delta_pitch){
    const GLfloat new_pitch = this->pitch + delta_pitch;

    // Handle overflows
    if (new_pitch > MAX_PITCH)
        this->pitch = MAX_PITCH;
    else if (new_pitch < MIN_PITCH)
        this->pitch = MIN_PITCH;
    else
        this-> pitch = new_pitch;

    this->vectorsFresh = false;
    this->viewFresh = false;
}

void Camera::yawCamera(const GLfloat& delta_yaw){
    this->yaw = this->yaw + delta_yaw;
    this->vectorsFresh = false;
    this->viewFresh = false;
}

void Camera::updateVectors(){
    const GLfloat yaw_radians = DegreesToRadians * this->yaw;
    const GLfloat pitch_radians = DegreesToRadians * this->pitch;

    this->direction = normalize(
        vec4(
            cos(yaw_radians) * cos(pitch_radians),
            sin(pitch_radians),
            sin(yaw_radians) * cos(pitch_radians),
            0.0
        )
    );
    this->vectorsFresh = true;
}


void Camera::updateViewMatrix(){
    if(!this->vectorsFresh)
        this->updateVectors();

    this->viewMatrix = LookAt(this->position, this->position + this->direction, this->up);
    this->viewFresh = true;
}


const vec4& Camera::getDirection(){
    if(!this->vectorsFresh)
        this->updateVectors();

    return this->direction;
}

const point4& Camera::getPosition(){
    return this->position;
}


const mat4& Camera::getViewMatrix() {
    if (!this->viewFresh)
        updateViewMatrix();

    return this->viewMatrix;
}