#ifndef __APP_H
#define __APP_H

#include "absapp.h"
#include "obj.h"
#include "camera.h"
#include "light.h"
#include "terrain.h"
#include "wall.h"
#include "collision.h"

/* Possible colors. */
enum color_t {c_black, c_red, c_yellow, c_green, c_blue, c_magenta, c_white, c_cyan, c_gray};

/* Draw modes. */
enum draw_mode_t {WIREFRAME_MODE = 0, SHADING_MODE = 1, TEXTURE_MODE = 2, TEXTURE_SHADING_MODE = 3};

/* Reflection models */
enum reflection_model_t {PHONG_MODEL = 0, MODIFIED_PHONG_MODEL = 1};

/* Projection modes. */
enum proj_mode_t {ORTHOGRAPHIC_MODE, PERSPECTIVE_MODE};


class GameApplication : virtual public Application {
    protected:
        static const char * CAMERA_POSITION_NAME;

        /* Amount of zoom for each ket event. */
        static const GLfloat ZOOM_DELTA;

        /* Amount of rotation for each key event. */
        static const GLfloat ROTATION_DELTA;

        /* Amount of movement for each key event. */
        static const GLfloat MOVE_DELTA;

        /* Move period */
        static const long int MOVE_PERIOD;

        /* Default color */
        static const color_t DEFAULT_COLOR;

        /* Default draw mode */
        static const draw_mode_t DEFAULT_DRAW_MODE;

        /* Default reflection model */
        static const reflection_model_t DEFAULT_REFLECTION_MODEL;

        /* Default Projection mode */
        static const proj_mode_t DEFAULT_PROJECTION_MODE;

        /* Min point source distance */
        static const GLfloat MIN_POINT_SOURCE_DISTANCE;

        /* Point source distance step */
        static const GLfloat POINT_SOURCE_DISTANCE_STEP;

        /* uniform attribute name for color */
        static const char* COLOR_ATTRIB_NAME;

        /* uniform attribute name for draw mode */
        static const char* DRAW_MODE_ATTRIB_NAME;

        /* uniform attribute name for reflection model */
        static const char* REFLECTION_MODEL_ATTRIB_NAME;

        /* uniform location for camera */
        GLuint cameraPositionLocation;

        /* uniform location for color */
        GLuint colorLocation;

        /* uniform location for draw mode */
        GLuint drawModeLocation;

        /* uniform location for reflection model */
        GLuint reflectionModelLocation;

        /* current color */
        color_t currentColor;

        /* current projection mode */
        proj_mode_t projectionMode;

        
        /* when and where mouse was recieved. */
        long int lastMouseTimeMillis;
        int lastMouseX;
        int lastMouseY;
        bool mouseInside = false;
        bool mouseIsNew = true;

        bool mouseCaptured = false;

        /* move keys */
        bool moveKeys[4] =  {false, false, false, false};
        long int lastMoved = 0; // last moved variable for adjusting move speed.
        GLfloat moveSpeed = 0.01; // move distance on each millisecond

        GLfloat gravitySpeed = 0.05; // fake gravity.
        bool flyMode = false; // Able to fly. (no gravity)


        /* Terrain */
        TerrainUnit * terrainType = nullptr;
        Terrain * terrain = nullptr;

        /* Variable storing the objects. */
        vector<GLObject *> objects;
        
        /* Camera */
        Camera * camera = nullptr;
        bool cameraRotated = false;

        /* Lighting Manager */
        LightingManager * lightingManager = nullptr;

        /* Flash light ids */
        GLuint flashlightId;

        /* Point source distance */
        GLfloat pointSourceDistance;

        /* The amount camera is moved away from the object in the center */
        const GLfloat cameraHeight = 3.0;

        /* Collision detection related */
        CollisionDetector collisionDetector;
        AxisAlignedBoundingBox * playerBox = nullptr;

        /* Game content */
        static const unsigned int NUM_CHECKPOINTS = 5;
        unsigned int nextCheckpoint = 0;
        point3 checkpoints[NUM_CHECKPOINTS];

        unsigned int checkpointTextures[NUM_CHECKPOINTS][2];
        GLObject * checkpointObjects[NUM_CHECKPOINTS];

        GLfloat checkpointLightHeight;
        GLuint checkpointLights[NUM_CHECKPOINTS];

        CollisionDetector checkpointDetector;

        void prepareCheckpoint(unsigned int checkpoint);
        void checkpointArrival();

        /* Wall related. */
        vector<Wall *> walls;

        /* initializes the walls */
        void initWalls();

        static const GLfloat& wallWidthScale;
        static const GLfloat& wallHeight;
        static const GLfloat& wallFilling;
        static const GLfloat& wallYCenter;
        static const GLfloat& wallTextureWidth;
        static const GLfloat& wallTextureHeight;

        unsigned int wallTextures[2] = {
            TextureManager::getInstance()->fromImageFile(string("assets/textures/wall_d.jpg")),
            TextureManager::getInstance()->fromImageFile(string("assets/textures/wall_s.jpg"))
        };

        void addHorizontalWallCollision(Wall * wall);
        void addVerticalWallCollision(Wall * wall);
        void changeWallDiffuseTextures(unsigned int tex);
        void changeWallSpecularTextures(unsigned int tex);

        virtual void updateViewMatrix();
        virtual void updateProjectionMatrix();

        void changeBackgroudColor(const color_t& color);
        void changeColor(const color_t& color);
        void changeDrawMode(const draw_mode_t& mode);
        void changeProjectionMode(const proj_mode_t& mode);
        void changeReflectionModel(const reflection_model_t& model);
        void switchLightOnOff(const GLuint& source_id);

        void rotateCamera(const GLfloat& yaw, const GLfloat& pitch);
        void moveCamera(const vec3& delta); // move the camera
        bool movePlayer(); // move player based on the keys pressed.

        void printInstructions();

        /* capture/free mouse pointer */
        void captureMouse();
        void freeMouse();

        /* initialize mouse location when the application starts */
        void initMouseLocation();

        /* initializes the flashlight */
        void initFlashlight();

        /* records the mouse attributes */
        void recordMouseLocation(int x, int y);

        virtual void idle();
        virtual void keyboardControl(unsigned char key, int x, int y);
        virtual void keyboardReleaseControl(unsigned char key, int x, int y);
        virtual void mouseEntryControl(int state);
        virtual void mousePassiveMotionControl(int x, int y);
        virtual void render();
        virtual void resize(int width, int height);
        virtual void specialKeyControl(int key, int x, int y);
        virtual void specialKeyReleaseControl(int key, int x, int y);


    public:
        explicit GameApplication(const std::string& title, const int width, const int height);
        virtual void init(int argc, char** argv);
};
#endif