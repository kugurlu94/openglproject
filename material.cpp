#include "material.h"

/* MaterialProperties Implementation */

const color4 MaterialProperties::DEFAULT_EMISSION = color4( 0.0, 0.0, 0.0, 1.0 );
const GLfloat MaterialProperties::DEFAULT_SHININESS = 100.0;


MaterialProperties::MaterialProperties(const unsigned int& diffuse, const unsigned int& specular, const color4& emission, const GLfloat& shininess)
    : diffuse(diffuse), specular(specular), emission(emission), shininess(shininess){}

unsigned int MaterialProperties::getDiffuse(){
    return this->diffuse;
}

unsigned int MaterialProperties::getSpecular(){
    return this->specular;
}

color4 MaterialProperties::getEmission(){
    return this->emission;
}

GLfloat MaterialProperties::getShininess(){
    return this->shininess;
}

void MaterialProperties::setDiffuse(const unsigned int& diffuse){
    this->diffuse = diffuse;
}

void MaterialProperties::setSpecular(const unsigned int& specular){
    this->specular = specular;
}

void MaterialProperties::setEmission(const color4& emission){
    this->emission = emission;
}

void MaterialProperties::setShininess(const GLfloat& mat_shininess){
    this->shininess = mat_shininess;
}
