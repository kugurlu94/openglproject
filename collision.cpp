#include "collision.h"

AxisAlignedBoundingBox::AxisAlignedBoundingBox(
        const GLfloat& minX, const GLfloat& minY, const GLfloat& minZ,
        const GLfloat& maxX, const GLfloat& maxY, const GLfloat& maxZ):
    minX(minX), minY(minY), minZ(minZ), maxX(maxX), maxY(maxY), maxZ(maxZ){}


void AxisAlignedBoundingBox::move(const vec3& move_vec){
    this->minX += move_vec.x;
    this->maxX += move_vec.x;
    this->minY += move_vec.y;
    this->maxY += move_vec.y;
    this->minZ += move_vec.z;
    this->maxZ += move_vec.z;
}

void AxisAlignedBoundingBox::moveX(const GLfloat& delta){
    this->minX += delta;
    this->maxX += delta;
}

void AxisAlignedBoundingBox::moveY(const GLfloat& delta){
    this->minY += delta;
    this->maxY += delta;
}

void AxisAlignedBoundingBox::moveZ(const GLfloat& delta){
    this->minZ += delta;
    this->maxZ += delta;
}



void CollisionDetector::addStationary(const AxisAlignedBoundingBox& box){
    this->stationaryBoxes.push_back(box);
}


/* Check for any intersection. True if there is intersection*/
bool CollisionDetector::checkIntersection(const AxisAlignedBoundingBox& moving){
    unsigned int i;
    size_t vecSize = this->stationaryBoxes.size();

    for(i = 0; i < vecSize; i++){
        const AxisAlignedBoundingBox& stationary = this->stationaryBoxes[i];
        // intersection check
        if ( (stationary.minX <= moving.maxX && stationary.maxX >= moving.minX) &&
             (stationary.minY <= moving.maxY && stationary.maxY >= moving.minY) &&
             (stationary.minZ <= moving.maxZ && stationary.maxZ >= moving.minZ) )
            return true;
    }

    return false;
}

void CollisionDetector::clear(){
    this->stationaryBoxes.clear();
}