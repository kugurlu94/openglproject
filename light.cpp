#include "light.h"

/* LightProperties Implementation */

const color4 LightProperties::DEFAULT_AMBIENT = color4( 0.0, 0.0, 0.0, 1.0 );
const color4 LightProperties::DEFAULT_DIFFUSE = color4( 1.0, 1.0, 1.0, 1.0 );
const color4 LightProperties::DEFAULT_SPECULAR = color4( 1.0, 1.0, 1.0, 1.0 );


LightProperties::LightProperties(const color4& ambient_c, const color4& diffuse_c, const color4& specular_c)
    : ambient(ambient_c), diffuse(diffuse_c), specular(specular_c){}

color4 LightProperties::getAmbient() const {
    return this->ambient;
}

color4 LightProperties::getDiffuse() const {
    return this->diffuse;
}

color4 LightProperties::getSpecular() const {
    return this->specular;
}

void LightProperties::setAmbient(const color4& ambient){
    this->ambient = ambient;
}

void LightProperties::setDiffuse(const color4& diffuse){
    this->diffuse = diffuse;
}

void LightProperties::setSpecular(const color4& specular){
    this->specular = specular;
}

/* Implementation of AttenuationProperties */
const GLfloat AttenuationProperties::DEFAULT_CONSTANT = 1.0;
const GLfloat AttenuationProperties::DEFAULT_LINEAR = 0.7;
const GLfloat AttenuationProperties::DEFAULT_QUADRATIC = 1.8;


AttenuationProperties::AttenuationProperties(
    const GLfloat& constant, const GLfloat& linear, const GLfloat& quadratic):
    constant(constant), linear(linear), quadratic(quadratic){}


/* Getters */
GLfloat AttenuationProperties::getConstant() const {
    return this->constant;
}

GLfloat AttenuationProperties::getLinear() const {
    return this->linear;
}

GLfloat AttenuationProperties::getQuadratic() const {
    return this->quadratic;
}


/* Setters */
void AttenuationProperties::setConstant(const GLfloat& constant){
    this->constant = constant;
}

void AttenuationProperties::setLinear(const GLfloat& linear){
    this->linear = linear;
}

void AttenuationProperties::setQuadratic(const GLfloat& quadratic){
    this->quadratic = quadratic;
}


/* Implementation of LightSource */ 
const LightProperties& LightSource::DEFAULT_LIGHT_PROPERTIES = LightProperties();


LightSource::LightSource(const LightProperties& l_properties)
    : properties(l_properties){}


const LightProperties& LightSource::getProperties() const {
    return this->properties;
}

void LightSource::setProperties(const LightProperties& l_properties){
    this->properties = l_properties;
}


/* Implementation of PointLightSource Implementation */

const AttenuationProperties& PointLightSource::DEFAULT_ATTENUATION = AttenuationProperties();
const point3& PointLightSource::DEFAULT_POSITION = point3(0.0, 0.0, 0.0);


PointLightSource::PointLightSource(
    const point3& l_position, const LightProperties& l_properties, const AttenuationProperties& attenuationProperties):
    LightSource(l_properties), position(l_position), attenuationProperties(attenuationProperties){}


const AttenuationProperties& PointLightSource::getAttenuationProperties() const {
    return this->attenuationProperties;
}

const point3& PointLightSource::getPosition() const {
    return this->position;
}

void PointLightSource::setAttenuationProperties(const AttenuationProperties& attenuation){
    this->attenuationProperties = attenuation;
}

void PointLightSource::setPosition(const point3& l_position){
    this->position = l_position;
}

LightSourceType PointLightSource::getType() const {
    return LightSourceType::POINT_SOURCE;
}

/* Implementation of DirectionalLightSource */

const vec3& DirectionalLightSource::DEFAULT_DIRECTION = vec3(0.0, 0.0, -1.0);


DirectionalLightSource::DirectionalLightSource(const vec3& direction, const LightProperties& l_properties):
    LightSource(l_properties), direction(direction){}

const vec3& DirectionalLightSource::getDirection() const {
    return this->direction;
}

void DirectionalLightSource::setDirection(const vec3& direction){
    this->direction = normalize(direction);
}

LightSourceType DirectionalLightSource::getType() const {
    return LightSourceType::DIRECTIONAL_SOURCE;
}

/* Implementation of SpotLight */

const vec3& SpotLightSource::DEFAULT_DIRECTION = vec3(0.0, 0.0, -1.0);
const GLfloat& SpotLightSource::DEFAULT_CUTOFF_ANGLE = 15.0;
const GLfloat& SpotLightSource::DEFAULT_OUTER_CUTOFF_ANGLE = 20.0;

SpotLightSource::SpotLightSource(
    const point3& l_position, const vec3& direction,
    const GLfloat& cutOffAngle, const GLfloat& outerCutOffAngle,
    const LightProperties& l_properties,
    const AttenuationProperties& attenuationProperties):
    PointLightSource(l_position, l_properties, attenuationProperties),
    direction(direction), cutOffAngle(cutOffAngle), outerCutOffAngle(outerCutOffAngle){}


const vec3& SpotLightSource::getDirection() const {
    return this->direction;
}

const GLfloat& SpotLightSource::getCutOffAngle() const {
    return this->cutOffAngle;
}

const GLfloat& SpotLightSource::getOuterCufOffAngle() const {
    return this->outerCutOffAngle;
}


void SpotLightSource::setDirection(const vec3& direction){
    this->direction = direction;
}

void SpotLightSource::setCutOffAngle(const GLfloat& cutOff){
    this->cutOffAngle = cutOff;
}

void SpotLightSource::setOuterCutOffAngle(const GLfloat& outerCutOff){
    this->outerCutOffAngle = outerCutOff;
}


LightSourceType SpotLightSource::getType() const{
    return LightSourceType::SPOT_SOURCE;
}


/* Implementation of LightingManager */

LightingManager::LightingManager(const GLuint& program):
    program(program), 
    point_enabledLocation(glGetUniformLocation(program, "pointLightSources[0].enabled")),
    point_constantLocation(glGetUniformLocation(program, "pointLightSources[0].constant")),
    point_linearLocation(glGetUniformLocation(program, "pointLightSources[0].linear")),
    point_quadraticLocation(glGetUniformLocation(program, "pointLightSources[0].quadratic")),
    point_ambientLocation(glGetUniformLocation(program, "pointLightSources[0].ambient")),
    point_diffuseLocation(glGetUniformLocation(program, "pointLightSources[0].diffuse")),
    point_specularLocation(glGetUniformLocation(program, "pointLightSources[0].specular")),
    point_positionLocation(glGetUniformLocation(program, "pointLightSources[0].position")),
    point_locationOffset(8),
    dir_enabledLocation(glGetUniformLocation(program, "directionalLightSources[0].enabled")),
    dir_ambientLocation(glGetUniformLocation(program, "directionalLightSources[0].ambient")),
    dir_diffuseLocation(glGetUniformLocation(program, "directionalLightSources[0].diffuse")),
    dir_specularLocation(glGetUniformLocation(program, "directionalLightSources[0].specular")),
    dir_directionLocation(glGetUniformLocation(program, "directionalLightSources[0].direction")),
    dir_locationOffset(5),
    spot_enabledLocation(glGetUniformLocation(program, "spotLightSources[0].enabled")),
    spot_constantLocation(glGetUniformLocation(program, "spotLightSources[0].constant")),
    spot_linearLocation(glGetUniformLocation(program, "spotLightSources[0].linear")),
    spot_quadraticLocation(glGetUniformLocation(program, "spotLightSources[0].quadratic")),
    spot_ambientLocation(glGetUniformLocation(program, "spotLightSources[0].ambient")),
    spot_diffuseLocation(glGetUniformLocation(program, "spotLightSources[0].diffuse")),
    spot_specularLocation(glGetUniformLocation(program, "spotLightSources[0].specular")),
    spot_positionLocation(glGetUniformLocation(program, "spotLightSources[0].position")),
    spot_directionLocation(glGetUniformLocation(program, "spotLightSources[0].direction")),
    spot_cutOffCosineLocation(glGetUniformLocation(program, "spotLightSources[0].cutOffCosine")),
    spot_outerCutOffCosineLocation(glGetUniformLocation(program, "spotLightSources[0].outerCutOffCosine")),
    spot_locationOffset(11),
    lastPointSourceLocation(glGetUniformLocation(program, "lastPointSource")),
    lastDirectionalSourceLocation(glGetUniformLocation(program, "lastDirectionalSource")),
    lastSpotSourceLocation(glGetUniformLocation(program, "lastSpotSource")),
    lastPointSource(-1),
    lastDirectionalSource(-1),
    lastSpotSource(-1)
{
    unsigned int i;
    for(i = 0; i < MAX_POINT_SOURCES; i++){
        this->pointStatuses[i] = false;
    }

    for(i = 0; i < MAX_DIRECTIONAL_SOURCES; i++){
        this->directionalStatuses[i] = false;
    }

    for(i = 0; i < MAX_SPOT_SOURCES; i++){
        this->spotStatuses[i] = false;
    }

    glUseProgram(this->program);
    glUniform1i(this->lastPointSourceLocation, this->lastPointSource);
    glUniform1i(this->lastDirectionalSourceLocation, this->lastDirectionalSource);
    glUniform1i(this->lastSpotSourceLocation, this->lastSpotSource);
    glUseProgram(0);
}


void LightingManager::allocateLightSource(const GLuint& source_id){
    if (source_id >= 0 && source_id < MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES + MAX_SPOT_SOURCES){ // in valid range.
        if (source_id < MAX_POINT_SOURCES){ // point light.
            int lightIndex = source_id;

            if(lightIndex <= lastPointSource){ // valid.
                // Obtain properties.
                const PointLightSource& l_source = this->pointSources[lightIndex];
                const LightProperties& l_properties = l_source.getProperties();
                const AttenuationProperties& attenuation = l_source.getAttenuationProperties();

                GLuint offset = lightIndex * this->point_locationOffset;

                // Set uniforms
                glUseProgram(this->program);
                glUniform1ui(this->point_enabledLocation + offset, this->pointStatuses[lightIndex]);
                glUniform1f(this->point_constantLocation + offset, attenuation.getConstant());
                glUniform1f(this->point_linearLocation + offset, attenuation.getLinear());
                glUniform1f(this->point_quadraticLocation + offset, attenuation.getQuadratic());
                glUniform4fv(this->point_ambientLocation + offset, 1, l_properties.getAmbient());
                glUniform4fv(this->point_diffuseLocation + offset, 1, l_properties.getDiffuse());
                glUniform4fv(this->point_specularLocation + offset, 1, l_properties.getSpecular());
                glUniform3fv(this->point_positionLocation + offset, 1, l_source.getPosition());
                glUseProgram(0);
            }

        } else if(source_id < MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES){ // directional light
            int lightIndex = source_id - MAX_POINT_SOURCES;

            if(lightIndex <= lastDirectionalSource){ // valid.
                const DirectionalLightSource& l_source = this->directionalSources[lightIndex];
                const LightProperties& l_properties = l_source.getProperties();

                GLuint offset = lightIndex * this->dir_locationOffset;

                // Set uniforms
                glUseProgram(this->program);
                glUniform1ui(this->dir_enabledLocation + offset, this->directionalStatuses[lightIndex]);
                glUniform4fv(this->dir_ambientLocation + offset, 1, l_properties.getAmbient());
                glUniform4fv(this->dir_diffuseLocation + offset, 1, l_properties.getDiffuse());
                glUniform4fv(this->dir_specularLocation + offset, 1, l_properties.getSpecular());
                glUniform3fv(this->dir_directionLocation + offset, 1, l_source.getDirection());
                glUseProgram(0);
            }
        } else if (source_id < MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES + MAX_SPOT_SOURCES){ // spot light
            int lightIndex = source_id - (MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES);

            if(lightIndex <= lastSpotSource){ // valid
                // Obtain properties.
                const SpotLightSource& l_source = this->spotSources[lightIndex];
                const LightProperties& l_properties = l_source.getProperties();
                const AttenuationProperties& attenuation = l_source.getAttenuationProperties();

                GLuint offset = lightIndex * this->spot_locationOffset;

                // Set uniforms
                glUseProgram(this->program);
                glUniform1ui(this->spot_enabledLocation + offset, this->spotStatuses[lightIndex]);
                glUniform1f(this->spot_constantLocation + offset, attenuation.getConstant());
                glUniform1f(this->spot_linearLocation + offset, attenuation.getLinear());
                glUniform1f(this->spot_quadraticLocation + offset, attenuation.getQuadratic());
                glUniform4fv(this->spot_ambientLocation + offset, 1, l_properties.getAmbient());
                glUniform4fv(this->spot_diffuseLocation + offset, 1, l_properties.getDiffuse());
                glUniform4fv(this->spot_specularLocation + offset, 1, l_properties.getSpecular());
                glUniform3fv(this->spot_positionLocation + offset, 1, l_source.getPosition());
                glUniform3fv(this->spot_directionLocation + offset, 1, l_source.getDirection());
                glUniform1f(this->spot_cutOffCosineLocation + offset, cos(DegreesToRadians * l_source.getCutOffAngle()));
                glUniform1f(this->spot_outerCutOffCosineLocation + offset, cos(DegreesToRadians * l_source.getOuterCufOffAngle()));
                glUseProgram(0);
            }
        }
    }
}



/* 
    Inserts and enables a light source, Returns an identifier to the source which might be called
    used for later calls to modification methods. Returns -1 if there are no empty light slot.
*/ 
GLuint LightingManager::addLightSource(const LightSource& l_source){
    GLuint source_id;

    LightSourceType l_type = l_source.getType();

    switch(l_type){
        case LightSourceType::POINT_SOURCE:
        {
            if(this->lastPointSource < MAX_POINT_SOURCES - 1){
                this->pointSources[++this->lastPointSource] = dynamic_cast<const PointLightSource&>(l_source);

                glUseProgram(this->program);
                glUniform1i(this->lastPointSourceLocation, this->lastPointSource);
                glUseProgram(0);

                source_id = this->lastPointSource;
            } else {
                source_id = -1;
            }
            break;
        }
        case LightSourceType::DIRECTIONAL_SOURCE:
        {
            if(this->lastDirectionalSource < MAX_DIRECTIONAL_SOURCES - 1){
                this->directionalSources[++this->lastDirectionalSource] = dynamic_cast<const DirectionalLightSource&>(l_source);

                glUseProgram(this->program);
                glUniform1i(this->lastDirectionalSourceLocation, this->lastDirectionalSource);
                glUseProgram(0);

                source_id = this->lastDirectionalSource + MAX_POINT_SOURCES;
            } else {
                source_id = -1;
            }
            break;
        }
        case LightSourceType::SPOT_SOURCE:
        {
            if(this->lastSpotSource < MAX_SPOT_SOURCES -1){
                this->spotSources[++this->lastSpotSource] = dynamic_cast<const SpotLightSource&>(l_source);
                
                glUseProgram(this->program);
                glUniform1i(this->lastSpotSourceLocation, this->lastSpotSource);
                glUseProgram(0);

                source_id = this->lastSpotSource + MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES;
            } else {
                source_id = -1;
            }
            break;
        }
        default:
        {
            source_id = -1;
            break;
        }

    }
    
    if(source_id != GLuint(-1)){ // success
        this->allocateLightSource(source_id);
        this->enableLightSource(source_id);
    }

    return source_id;
}


/*
    Enables the current light source with given source_id
    Updates uniform variables in the given program.
*/

void LightingManager::enableLightSource(const GLuint& source_id){
    if (source_id >= 0 && source_id < MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES + MAX_SPOT_SOURCES){ // in valid range.
        if (source_id < MAX_POINT_SOURCES){ // point light.
            int lightIndex = source_id;

            if(lightIndex <= lastPointSource){ // valid.
                GLuint offset = lightIndex * this->point_locationOffset;

                this->pointStatuses[lightIndex] = true;

                glUseProgram(this->program);
                glUniform1ui(this->point_enabledLocation + offset, true);
                glUseProgram(0);

            }

        } else if(source_id < MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES){ // directional light
            int lightIndex = source_id - MAX_POINT_SOURCES;

            if(lightIndex <= lastDirectionalSource){ // valid.
                GLuint offset = lightIndex * this->dir_locationOffset;

                this->directionalStatuses[lightIndex] = true;

                glUseProgram(this->program);
                glUniform1ui(this->dir_enabledLocation + offset, true);
                glUseProgram(0);
            }
        } else if (source_id < MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES + MAX_SPOT_SOURCES){ // spot light
            int lightIndex = source_id - (MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES);

            if(lightIndex <= lastSpotSource){ // valid
                GLuint offset = lightIndex * this->spot_locationOffset;

                this->spotStatuses[lightIndex] = true;

                glUseProgram(this->program);
                glUniform1ui(this->spot_enabledLocation + offset, true);
                glUseProgram(0);
            }
        }
    }
}

/*
    Disables the current light source with given source_id
    Updates uniform variables in the given program.
*/

void LightingManager::disableLightSource(const GLuint& source_id){
    if (source_id >= 0 && source_id < MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES + MAX_SPOT_SOURCES){ // in valid range.
        if (source_id < MAX_POINT_SOURCES){ // point light.
            int lightIndex = source_id;

            if(lightIndex <= lastPointSource){ // valid.
                GLuint offset = lightIndex * this->point_locationOffset;

                this->pointStatuses[lightIndex] = false;

                glUseProgram(this->program);
                glUniform1ui(this->point_enabledLocation + offset, false);
                glUseProgram(0);

            }

        } else if(source_id < MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES){ // directional light
            int lightIndex = source_id - MAX_POINT_SOURCES;

            if(lightIndex <= lastDirectionalSource){ // valid.
                GLuint offset = lightIndex * this->dir_locationOffset;

                this->directionalStatuses[lightIndex] = false;

                glUseProgram(this->program);
                glUniform1ui(this->dir_enabledLocation + offset, false);
                glUseProgram(0);
            }
        } else if (source_id < MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES + MAX_SPOT_SOURCES){ // spot light
            int lightIndex = source_id - (MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES);

            if(lightIndex <= lastSpotSource){ // valid
                GLuint offset = lightIndex * this->spot_locationOffset;

                this->spotStatuses[lightIndex] = false;

                glUseProgram(this->program);
                glUniform1ui(this->spot_enabledLocation + offset, false);
                glUseProgram(0);
            }
        }
    }
}

/*
    Checks the current light source with given source_id
*/

bool LightingManager::lightEnabled(const GLuint& source_id){
    bool result = false;
    if (source_id >= 0 && source_id < MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES + MAX_SPOT_SOURCES){ // in valid range.
        if (source_id < MAX_POINT_SOURCES){ // point light.
            int lightIndex = source_id;

            if(lightIndex <= lastPointSource){ // valid.
                result = this->pointStatuses[lightIndex];
            }
        } else if(source_id < MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES){ // directional light
            int lightIndex = source_id - MAX_POINT_SOURCES;

            if(lightIndex <= lastDirectionalSource){ // valid.
                result = this->directionalStatuses[lightIndex];
            }
        } else if (source_id < MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES + MAX_SPOT_SOURCES){ // spot light
            int lightIndex = source_id - (MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES);

            if(lightIndex <= lastSpotSource){ // valid
                result = this->spotStatuses[lightIndex];
            }
        }
    }
    return result;
}

void LightingManager::setProperties(const GLuint& source_id, const LightProperties& l_properties){
    if (source_id >= 0 && source_id < MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES + MAX_SPOT_SOURCES){ // in valid range.
        if (source_id < MAX_POINT_SOURCES){ // point light.
            int lightIndex = source_id;

            if(lightIndex <= lastPointSource){ // valid.
                this->pointSources[lightIndex].setProperties(l_properties);
                this->allocateLightSource(source_id);
            }
        } else if(source_id < MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES){ // directional light
            int lightIndex = source_id - MAX_POINT_SOURCES;

            if(lightIndex <= lastDirectionalSource){ // valid.
                this->directionalSources[lightIndex].setProperties(l_properties);
                this->allocateLightSource(source_id);
            }
        } else if (source_id < MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES + MAX_SPOT_SOURCES){ // spot light
            int lightIndex = source_id - (MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES);

            if(lightIndex <= lastSpotSource){ // valid
                this->spotSources[lightIndex].setProperties(l_properties);
                this->allocateLightSource(source_id);
            }
        }
    }
}

/* 
    Sets changes the position of the light source with given id.
    Must be a point light source.
*/
void LightingManager::setPosition(const GLuint& source_id, const point3& position){
    if (source_id >= 0 && source_id < MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES + MAX_SPOT_SOURCES){ // in valid range.
        if (source_id < MAX_POINT_SOURCES){ // point light.
            int lightIndex = source_id;

            if(lightIndex <= lastPointSource){ // valid.
                this->pointSources[lightIndex].setPosition(position);
                GLuint offset = lightIndex * this->point_locationOffset;

                // Set uniforms
                glUseProgram(this->program);
                glUniform3fv(this->point_positionLocation + offset, 1, this->pointSources[lightIndex].getPosition());
                glUseProgram(0);
            }
        } else if(source_id < MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES){ // directional light
            // no position
        } else if (source_id < MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES + MAX_SPOT_SOURCES){ // spot light
            int lightIndex = source_id - (MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES);

            if(lightIndex <= lastSpotSource){ // valid
                this->spotSources[lightIndex].setPosition(position);
                GLuint offset = lightIndex * this->spot_locationOffset;

                // Set uniforms
                glUseProgram(this->program);
                glUniform3fv(this->spot_positionLocation + offset, 1, this->spotSources[lightIndex].getPosition());
                glUseProgram(0);
            }
        }
    }
}

void LightingManager::setDirection(const GLuint& source_id, const vec3& direction){
    if (source_id >= 0 && source_id < MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES + MAX_SPOT_SOURCES){ // in valid range.
        if (source_id < MAX_POINT_SOURCES){ // point light.
            // no direction
        } else if(source_id < MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES){ // directional light
            int lightIndex = source_id - MAX_POINT_SOURCES;

            if(lightIndex <= lastDirectionalSource){ // valid
                this->directionalSources[lightIndex].setDirection(direction);

                // Set uniforms
                GLuint offset = lightIndex * this->dir_locationOffset;
                glUseProgram(this->program);
                glUniform3fv(this->dir_directionLocation + offset, 1, this->directionalSources[lightIndex].getDirection());
                glUseProgram(0);
            }
        } else if (source_id < MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES + MAX_SPOT_SOURCES){ // spot light
            int lightIndex = source_id - (MAX_POINT_SOURCES + MAX_DIRECTIONAL_SOURCES);

            if(lightIndex <= lastSpotSource){ // valid
                this->spotSources[lightIndex].setDirection(direction);

                // set uniforms
                GLuint offset = lightIndex * this->spot_locationOffset;
                glUseProgram(this->program);
                glUniform3fv(this->spot_directionLocation + offset, 1, this->spotSources[lightIndex].getDirection());
                glUseProgram(0);
            }
        }
    }
}
