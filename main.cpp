#include <string>
#include "app.h"

/*
    Note: The project is made to be used on linux platforms.
    There might be incompatiblity issues that I have not addressed.
    I used Ubuntu 14.04, X.Org 1.17.2 and OpenGL 4.5.
*/

/* 
    The entry point of the application.
*/
int main(int argc, char* argv[]){
    const std::string& windowTitle("Maze");
    GameApplication gameApp(windowTitle, 1024, 1024);
    gameApp.init(argc, argv);
    gameApp.start();
}
