#ifndef __COLLISION_H
#define __COLLISION_H

#include <Angel.h>
#include <vector>


struct AxisAlignedBoundingBox {
    GLfloat minX;
    GLfloat minY;
    GLfloat minZ;

    GLfloat maxX;
    GLfloat maxY;
    GLfloat maxZ;

    AxisAlignedBoundingBox(
        const GLfloat& minX, const GLfloat& minY, const GLfloat& minZ,
        const GLfloat& maxX, const GLfloat& maxY, const GLfloat& maxZ);

    void move(const vec3& move_vec);
    void moveX(const GLfloat& delta);
    void moveY(const GLfloat& delta);
    void moveZ(const GLfloat& delta);
};

class CollisionDetector {
    protected:
        vector<AxisAlignedBoundingBox> stationaryBoxes;

    public:
        void addStationary(const AxisAlignedBoundingBox& box);
        bool checkIntersection(const AxisAlignedBoundingBox& moving);
        void clear();
};



#endif