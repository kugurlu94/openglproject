#include "app.h"


/* Rotation/zoom constant */
const GLfloat GameApplication::ZOOM_DELTA = 0.1;
const GLfloat GameApplication::ROTATION_DELTA = 2.0;
const GLfloat GameApplication::MOVE_DELTA = 0.1;
const long int GameApplication::MOVE_PERIOD = 16;
const color_t GameApplication::DEFAULT_COLOR = color_t::c_white;

/* Default state variables */
const draw_mode_t GameApplication::DEFAULT_DRAW_MODE = draw_mode_t::TEXTURE_SHADING_MODE;
const reflection_model_t GameApplication::DEFAULT_REFLECTION_MODEL = reflection_model_t::MODIFIED_PHONG_MODEL;
const proj_mode_t GameApplication::DEFAULT_PROJECTION_MODE = proj_mode_t::PERSPECTIVE_MODE;
const GLfloat GameApplication::MIN_POINT_SOURCE_DISTANCE = 1.2;
const GLfloat GameApplication::POINT_SOURCE_DISTANCE_STEP = 0.1;

/* Shader attribute names */
const char* GameApplication::CAMERA_POSITION_NAME = "cameraPosition";
const char* GameApplication::COLOR_ATTRIB_NAME = "Color";
const char* GameApplication::DRAW_MODE_ATTRIB_NAME = "DrawMode";
const char* GameApplication::REFLECTION_MODEL_ATTRIB_NAME = "ReflectionModel";

/* Wall constants */
const GLfloat& GameApplication::wallWidthScale = 4.0;
const GLfloat& GameApplication::wallHeight = 5.0;
const GLfloat& GameApplication::wallFilling = 0.1;
const GLfloat& GameApplication::wallYCenter = wallHeight / 2.0;
const GLfloat& GameApplication::wallTextureWidth = 2.0;
const GLfloat& GameApplication::wallTextureHeight = 2.0;


GameApplication::GameApplication(const std::string& title, const int width, const int height) :
            Application(title, width, height){}


void GameApplication::init(int argc, char** argv){
    Application::init(argc, argv);

    cout << "Welcome to Maze Game." << endl;
    cout << "Wait for textures to load..." << endl;

    /* set colorLocation and initial color value */
    this->cameraPositionLocation = glGetUniformLocation(this->program, CAMERA_POSITION_NAME);
    this->colorLocation = glGetUniformLocation(this->program, COLOR_ATTRIB_NAME);
    this->drawModeLocation = glGetUniformLocation(this->program, DRAW_MODE_ATTRIB_NAME);
    this->reflectionModelLocation = glGetUniformLocation(this->program, REFLECTION_MODEL_ATTRIB_NAME);

    unsigned int terrainTextures[2] = {
        TextureManager::getInstance()->fromImageFile(string("assets/textures/desert_mud_d.jpg")),
        TextureManager::getInstance()->fromImageFile(string("assets/textures/desert_mud_s.jpg"))
    };

    this->terrainType = new SimpleTerrainUnit(this->program, 128.0, 4.0);
    this->terrainType->setDiffuseTexture(terrainTextures[0]);
    this->terrainType->setSpecularTexture(terrainTextures[1]);
    this->terrain = new SimpleTerrain(this->terrainType, 2, 2, point3(0.0, 0.0, 0.0));
    this->collisionDetector.addStationary(
        AxisAlignedBoundingBox(
            -128.0,  -128.0, -128.0,   
             128.0,    0.05,  128.0
        )
    );

    /*

    unsigned int objectTextures[6][2] = {
        {
            TextureManager::getInstance()->fromImageFile(string("assets/textures/mntn_canyon_d.jpg")),
            0
        },
        {
            TextureManager::getInstance()->fromImageFile(string("assets/textures/jungle_stone_d.jpg")),
            TextureManager::getInstance()->fromImageFile(string("assets/textures/jungle_stone_s.jpg"))
        },
        {
            TextureManager::getInstance()->fromImageFile(string("assets/textures/island_sand_d.jpg")),
            0
        },
        {
            TextureManager::getInstance()->fromImageFile(string("assets/textures/ground_mud_d.jpg")),
            TextureManager::getInstance()->fromImageFile(string("assets/textures/ground_mud_s.jpg"))
        },
        {
            TextureManager::getInstance()->fromImageFile(string("assets/textures/snow_rough_d.jpg")),
            TextureManager::getInstance()->fromImageFile(string("assets/textures/snow_rough_s.jpg"))
        },
        {
            TextureManager::getInstance()->fromImageFile(string("assets/textures/savanna_yellow_d.jpg")),
            TextureManager::getInstance()->fromImageFile(string("assets/textures/savanna_yellow_s.jpg"))
        }
    };

    int i;
    for(i=0; i < 100; i++){
        GLObject * object = new GLCube(this->program);
        object->setPosition(point3(1.0 * ((i % 13) - 6), 1.0 * ((i % 7) + 1), 1.5 * ((i % 17) - 7)));
        object->scale(0.3 * (i % 5));
        object->rotateX(15.0 * (i % 15));
        object->rotateY(15.0 * (i % 17));
        object->rotateZ(15.0 * (i % 19));
        object->setDiffuseTexture(objectTextures[(i % 6)][0]);
        object->setSpecularTexture(objectTextures[(i % 6)][1]);
        this->objects.push_back(object);
    }
    */


    this->camera = new Camera();
    this->lightingManager = new LightingManager(this->program);

    this->moveCamera(vec3(2.0, this->cameraHeight, -6.0));
    this->rotateCamera(180.0, 0.0);
    this->updateViewMatrix();

    // Initilize playerBox
    const vec4& cameraPosition = this->camera->getPosition();
    this->playerBox = new AxisAlignedBoundingBox(
        cameraPosition.x - 0.5, 0.1,                                         cameraPosition.z - 0.5,
        cameraPosition.x + 0.5, cameraPosition.y + this->cameraHeight * 0.2, cameraPosition.z + 0.5
    );

    /* directional light*/
    this->lightingManager->addLightSource(
        DirectionalLightSource(
            normalize(vec3(0.0, -1.0, -0.5)),
            LightProperties(
                color4(0.0, 0.0, 0.0, 1.0),
                color4(0.25, 0.25, 0.25, 1.0),
                color4(0.25, 0.25, 0.25, 1.0)
            )
        )
    );
    
    this->initFlashlight();
    this->lightingManager->disableLightSource(this->flashlightId);

    this->initWalls();

    this->changeColor(DEFAULT_COLOR);
    this->changeDrawMode(DEFAULT_DRAW_MODE);
    this->changeProjectionMode(DEFAULT_PROJECTION_MODE);
    this->changeReflectionModel(DEFAULT_REFLECTION_MODEL);

    /* Game content */
    this->nextCheckpoint = 0;
    this->checkpoints[0] = point3(2.5, 0.0, -1.5) * wallWidthScale;
    this->checkpoints[1] = point3(4.5, 0.0, -5.5) * wallWidthScale;
    this->checkpoints[2] = point3(2.5, 0.0, -7.5) * wallWidthScale;
    this->checkpoints[3] = point3(4.5, 0.0, -7.5) * wallWidthScale;
    this->checkpoints[4] = point3(9.5, 0.0, -8.5) * wallWidthScale;

    this->checkpointTextures[0][0] = TextureManager::getInstance()->fromImageFile(string("assets/textures/jungle_stone_d.jpg"));
    this->checkpointTextures[0][1] = TextureManager::getInstance()->fromImageFile(string("assets/textures/jungle_stone_s.jpg"));

    this->checkpointTextures[1][0] = TextureManager::getInstance()->fromImageFile(string("assets/textures/ground_mud_d.jpg"));
    this->checkpointTextures[1][1] = TextureManager::getInstance()->fromImageFile(string("assets/textures/ground_mud_s.jpg"));

    this->checkpointTextures[2][0] = TextureManager::getInstance()->fromImageFile(string("assets/textures/snow_rough_d.jpg"));
    this->checkpointTextures[2][1] = TextureManager::getInstance()->fromImageFile(string("assets/textures/snow_rough_s.jpg"));

    this->checkpointTextures[3][0] = TextureManager::getInstance()->fromImageFile(string("assets/textures/savanna_yellow_d.jpg"));
    this->checkpointTextures[3][1] = TextureManager::getInstance()->fromImageFile(string("assets/textures/savanna_yellow_s.jpg"));

    this->checkpointTextures[4][0] = this->wallTextures[0];
    this->checkpointTextures[4][1] = this->wallTextures[1];


    this->checkpointLightHeight = 6.0;
    unsigned int i;
    for(i = 0; i < NUM_CHECKPOINTS - 1; i++){
        this->checkpointLights[i] = this->lightingManager->addLightSource(
            PointLightSource(
                point3(checkpoints[i].x, this->checkpointLightHeight, checkpoints[i].z),
                LightProperties(
                    color4(0.002, 0.002, 0.002, 1.0),
                    color4(1.0, 1.0, 1.0, 1.0),
                    color4(1.0, 1.0, 1.0, 1.0)
                ),
                AttenuationProperties(1.0, 0.22, 0.0019)
            )
        );
        this->lightingManager->disableLightSource(this->checkpointLights[i]);
        this->checkpointObjects[i] = nullptr;
        this->checkpointObjects[i] = new GLCube(this->program);
        this->checkpointObjects[i]->setPosition(point3(checkpoints[i].x, this->checkpointLightHeight, checkpoints[i].z));
        this->checkpointObjects[i]->scale(0.2);
        this->checkpointObjects[i]->setDiffuseTexture(this->checkpointTextures[i][0]);
        this->checkpointObjects[i]->setSpecularTexture(this->checkpointTextures[i][1]);
    }

    /* Final light is a spotlight */
    this->checkpointLights[NUM_CHECKPOINTS - 1] = this->lightingManager->addLightSource(
        SpotLightSource(
            point3(checkpoints[NUM_CHECKPOINTS - 1].x, this->checkpointLightHeight, checkpoints[NUM_CHECKPOINTS - 1].z),
            vec3(0.0, -1.0, 0.0),
            10.0, 12.5,
            LightProperties(
                color4(0.0, 0.0, 0.0, 1.0),
                color4(1.0, 1.0, 1.0, 1.0),
                color4(1.0, 1.0, 1.0, 1.0)
            ),
            AttenuationProperties(1.0, 0.0014, 0.000007)
        )
    );

    // Create model suprise after finish
    unsigned int modelTexture = TextureManager::getInstance()->fromPPM(string("assets/textures/texture.ppm"));
    GLOffXObject * model = new GLOffXObject(this->program, string("assets/shapeX.offx"));
    model->setDiffuseTexture(modelTexture);
    model->setPosition(point3(checkpoints[i].x - wallWidthScale, wallHeight * 0.5 - 0.5, checkpoints[i].z));
    model->rotateY(90.0);
    model->scale(1.8);
    
    this->checkpointObjects[NUM_CHECKPOINTS - 1] = model;
    
    this->prepareCheckpoint(this->nextCheckpoint);

    this->captureMouse();
    this->printInstructions();
    
    /* Extra for presentation */
    glutFullScreen();
    /*
    this->checkpointLights[i] = this->lightingManager->addLightSource(
        PointLightSource(
            point3(5.0 * wallWidthScale, 10.0, -5.0 * wallWidthScale),
            LightProperties(
                color4(0.002, 0.002, 0.002, 1.0),
                color4(1.0, 1.0, 1.0, 1.0),
                color4(1.0, 1.0, 1.0, 1.0)
            ),
            AttenuationProperties(1.0, 0.0, 0.000)
        )
    );
    */

}


/*
    Updates the view matrix.
    Must be called on each camera change.
*/
void GameApplication::updateViewMatrix(){
    if (this->camera != nullptr){
        this->viewMatrix = this->camera->getViewMatrix();
    }
    
    glUseProgram(program);
    glUniformMatrix4fv(this->viewMatrixLocation, 1, GL_TRUE, this->viewMatrix);
    glUseProgram(0);

    glutPostRedisplay();
}


/*
    Updates the projection matrix.
    Must be called on changes in clipping volume.
*/
void GameApplication::updateProjectionMatrix(){
    GLfloat aspect = ((GLfloat) this->width / (GLfloat) this->height);

    if (this->projectionMode == proj_mode_t::PERSPECTIVE_MODE){
        if(aspect <= 1.0)
            this->projectionMatrix = Perspective(45.0, aspect, 0.1, 1024.0);
        else
            this->projectionMatrix = Perspective(45.0, aspect, 0.1, 1024.0);
    } else if (this->projectionMode == proj_mode_t::ORTHOGRAPHIC_MODE){
        if (aspect <= 1.0)
            this->projectionMatrix = Ortho( -1.0, 1.0, -1.0  / aspect, 1.0 / aspect, 0.0, (GLfloat) 1024.0);
        else
            this->projectionMatrix = Ortho( -1.0 * aspect, 1.0 * aspect, -1.0, 1.0, 0.0, (GLfloat) 1024.0);
    }

    glUseProgram(this->program);
    glUniformMatrix4fv(this->projectionMatrixLocation, 1, GL_TRUE, this->projectionMatrix);
    glUseProgram(0);

    glutPostRedisplay();
}

/* Changes the backgroud color */
void GameApplication::changeBackgroudColor(const color_t& color){
    switch(color){
        case c_black   : this->setClearColor(color4( 0.0, 0.0, 0.0, 1.0 )); break;
        case c_gray    : this->setClearColor(color4( 0.5, 0.5, 0.5, 1.0 )); break;
        case c_white   : this->setClearColor(color4( 1.0, 1.0, 1.0, 1.0 )); break;
        default        : break;
    }
    glutPostRedisplay();
}


/* Changes the current color. */
void GameApplication::changeColor(const color_t& color){
    this->currentColor = color;
    color4 color_vec;

    switch(this->currentColor)
    {
        case c_black   : color_vec = color4( 0.0, 0.0, 0.0, 1.0 ); break;
        case c_red     : color_vec = color4( 1.0, 0.0, 0.0, 1.0 ); break;
        case c_yellow  : color_vec = color4( 1.0, 1.0, 0.0, 1.0 ); break;
        case c_green   : color_vec = color4( 0.0, 1.0, 0.0, 1.0 ); break;
        case c_blue    : color_vec = color4( 0.0, 0.0, 1.0, 1.0 ); break;
        case c_magenta : color_vec = color4( 1.0, 0.0, 1.0, 1.0 ); break;
        case c_white   : color_vec = color4( 1.0, 1.0, 1.0, 1.0 ); break;
        case c_cyan    : color_vec = color4( 0.0, 1.0, 1.0, 1.0 ); break;
        default        : color_vec = color4( 1.0, 1.0, 1.0, 1.0 ); break;
    }

    glUseProgram(program);
    glUniform4fv(colorLocation, 1, color_vec);
    glUseProgram(0);

    glutPostRedisplay();
}

/* Changes the draw mode */
void GameApplication::changeDrawMode(const draw_mode_t& mode){
    glUseProgram(this->program);
    
    if (mode == draw_mode_t::WIREFRAME_MODE){
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        glUniform1ui(this->drawModeLocation, (GLuint) draw_mode_t::WIREFRAME_MODE);
    } else if (mode == draw_mode_t::SHADING_MODE){
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glUniform1ui(this->drawModeLocation, (GLuint) draw_mode_t::SHADING_MODE);
    } else if (mode == draw_mode_t::TEXTURE_MODE){
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glUniform1ui(this->drawModeLocation, (GLuint) draw_mode_t::TEXTURE_MODE);
    } else if (mode == draw_mode_t::TEXTURE_SHADING_MODE){
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glUniform1ui(this->drawModeLocation, (GLuint) draw_mode_t::TEXTURE_SHADING_MODE);
    }

    glUseProgram(0);

    glutPostRedisplay();
}


/* Changes the projetion mode */
void GameApplication::changeProjectionMode(const proj_mode_t& mode){
    this->projectionMode = mode;
    this->updateProjectionMatrix();
}

/* Changes reflection mode */
void GameApplication::changeReflectionModel(const reflection_model_t& model){
    glUseProgram(this->program);
    glUniform1ui(this->reflectionModelLocation, (GLuint) model);
    glUseProgram(0);
    glutPostRedisplay();
}


/* Light source switch */
void GameApplication::switchLightOnOff(const GLuint& source_id){
    if(this->lightingManager->lightEnabled(source_id))
        this->lightingManager->disableLightSource(source_id);
    else
        this->lightingManager->enableLightSource(source_id);

    glutPostRedisplay();
}

void GameApplication::rotateCamera(const GLfloat& yaw, const GLfloat& pitch){
    if(this->camera != nullptr){
        this->camera->yawCamera(yaw);
        this->camera->pitchCamera(pitch);
        this->cameraRotated = true;

        const vec4& cameraDirection = this->camera->getDirection();
        const vec3& cameraV3Direction = vec3(cameraDirection.x, cameraDirection.y, cameraDirection.z);

        if(this->lightingManager != nullptr)
            this->lightingManager->setDirection(this->flashlightId, cameraV3Direction);
    }
}

void GameApplication::moveCamera(const vec3& delta){
    if(this->camera != nullptr){
        this->camera->move(delta);
        const vec4& cameraPosition = this->camera->getPosition();
        const vec3& cameraV3Position =  vec3(cameraPosition.x, cameraPosition.y, cameraPosition.z);

        glUseProgram(this->program);
        glUniform3fv(this->cameraPositionLocation, 1, cameraV3Position);
        glUseProgram(0);

        if(this->lightingManager != nullptr)
            this->lightingManager->setPosition(this->flashlightId, cameraV3Position);
    }
}


bool GameApplication::movePlayer(){
    // calculate move direction.
    bool moved = false;
    long int oldLastMoved = this->lastMoved;
    long int newLastMoved = glutGet(GLUT_ELAPSED_TIME);

    if(oldLastMoved > 0){ // not first call
        long int moveTimeDelta = newLastMoved - oldLastMoved;
        if(moveTimeDelta >= MOVE_PERIOD){ // do the movement.
            const vec4& cameraDirection = this->camera->getDirection();

            vec3 moveVec = vec3(0.0, 0.0, 0.0);
            vec3 moveDirection = vec3(0.0, 0.0, 0.0);
            vec3 moveFront;
            if(!this->flyMode){
                moveFront = normalize(vec3(cameraDirection.x, 0.0, cameraDirection.z));
                moveVec += (this->gravitySpeed * moveTimeDelta) * vec3(0.0, -1.0, 0.0);
                moved = true;
            } else {
                moveFront = normalize(vec3(cameraDirection.x, cameraDirection.y, cameraDirection.z));
            }

            if(moveKeys[0] || moveKeys[1] || moveKeys[2] || moveKeys[3]){
                if(moveKeys[0] && !moveKeys[2]){
                    moveDirection += moveFront;
                }

                if(moveKeys[1] && !moveKeys[3]){
                    moveDirection += -1.0 * cross(moveFront, vec3(0.0, 1.0, 0.0));
                }

                if(!moveKeys[0] && moveKeys[2]){
                    moveDirection += -1.0 * moveFront;
                }
                
                if(!moveKeys[1] && moveKeys[3]){
                    moveDirection += cross(moveFront, vec3(0.0, 1.0, 0.0));
                }

                moved = true;
                moveVec += (this->moveSpeed * moveTimeDelta) * normalize(moveDirection);
            }

            if(moved){
                /* Collision Detection */
                bool collisionlessMove = false;
                vec3 nonCollisionMoveVec = vec3(0.0, 0.0, 0.0);

                AxisAlignedBoundingBox candidateBox = *this->playerBox;
                
                candidateBox.moveX(moveVec.x);
                if(!this->collisionDetector.checkIntersection(candidateBox)){
                    nonCollisionMoveVec.x = moveVec.x;
                    collisionlessMove = true;
                } else {
                    candidateBox.moveX(-moveVec.x);
                }

                candidateBox.moveY(moveVec.y);
                if(!this->collisionDetector.checkIntersection(candidateBox)){
                    nonCollisionMoveVec.y = moveVec.y;
                    collisionlessMove = true;
                } else {
                    candidateBox.moveY(-moveVec.y);
                }

                candidateBox.moveZ(moveVec.z);
                if(!this->collisionDetector.checkIntersection(candidateBox)){
                    nonCollisionMoveVec.z = moveVec.z;
                    collisionlessMove = true;
                } else {
                    candidateBox.moveZ(-moveVec.z);
                }

                if(collisionlessMove){
                    this->moveCamera(nonCollisionMoveVec);
                    *this->playerBox = candidateBox;
                } else {
                    moved = false;
                }
            }

            /* checkpoint check */
            if(moved && this->nextCheckpoint < NUM_CHECKPOINTS){
                if(this->checkpointDetector.checkIntersection(*this->playerBox)){
                    this->checkpointArrival();
                }
            }

            this->lastMoved = newLastMoved;
        }
    } else { // initialize
        this->lastMoved = newLastMoved;
    }

    return moved;
}


void GameApplication::printInstructions(){
    cout << "Try to find your way out..." << endl;
    cout << "Use mouse to look around." << endl
         << "Use AWSD to move around." << endl
         << "Press L to switch flashlight on/off." << endl
         << "Press C to free/capture mouse." << endl
         << "(Cheat) Press G to remove gravity." << endl
         << "Press ESC to exit." << endl << endl;
}


void GameApplication::initFlashlight(){
    const vec4& cameraPosition = this->camera->getPosition();
    const vec3& cameraV3Position =  vec3(cameraPosition.x, cameraPosition.y, cameraPosition.z);

    const vec4& cameraDirection = this->camera->getDirection();
    const vec3& cameraV3Direction = vec3(cameraDirection.x, cameraDirection.y, cameraDirection.z);

    this->flashlightId = this->lightingManager->addLightSource(
        SpotLightSource(
            cameraV3Position,
            cameraV3Direction,
            5.0, 20.0,
            LightProperties(
                color4(0.0, 0.0, 0.0, 1.0),
                color4(1.0, 1.0, 1.0, 1.0),
                color4(1.0, 1.0, 1.0, 1.0)
            ),
            AttenuationProperties(1.0, 0.045, 0.0075)
        )
    );
}


void GameApplication::initMouseLocation(){
    int mouseX = glutGet(GLUT_WINDOW_WIDTH) / 2;
    int mouseY = glutGet(GLUT_WINDOW_HEIGHT) / 2;
    glutWarpPointer(mouseX, mouseY);
    this->mouseIsNew = true;
    this->recordMouseLocation(mouseX, mouseY);
}

void GameApplication::recordMouseLocation(int x, int y){
    this->lastMouseTimeMillis = glutGet(GLUT_ELAPSED_TIME);
    this->lastMouseX = x;
    this->lastMouseY = y;
}

void GameApplication::idle(){
    bool shouldUpdateViewMatrix = false;

    if(this->movePlayer())
        shouldUpdateViewMatrix = true;

    if(this->cameraRotated){
        shouldUpdateViewMatrix = true;
        this->cameraRotated = false;
    }

    if(shouldUpdateViewMatrix)
        this->updateViewMatrix();
}


/* Keyboard controls. */
void GameApplication::keyboardControl(unsigned char key, int x, int y){
    switch (key)
    {
        /* moving around */
        case 'w': case 'W':
            {
                this->moveKeys[0] = true;
                break;
            }
        case 'a': case 'A':
            {
                this->moveKeys[1] = true;
                break;
            } 
        case 's': case 'S':
            {
                this->moveKeys[2] = true;
                break;
            }
        case 'd': case 'D':
            {
                this->moveKeys[3] = true;
                break;
            }
        case 'g': case 'G':
            {
                this->flyMode = !this->flyMode;
                break;
            }
        case 'f': case 'F':
            {
                if(this->lightingManager->lightEnabled(this->flashlightId)){
                    this->lightingManager->disableLightSource(this->flashlightId);
                } else {
                    this->lightingManager->enableLightSource(this->flashlightId);
                }
                glutPostRedisplay();
                break;
            }
        case 'c': case 'C':
            {
                if(this->mouseCaptured)
                    this->freeMouse();
                else
                    this->captureMouse();
                break;
            }
        case 'h': case 'H':
            {
                this->printInstructions();
                break;
            }
        case 27:
            {
                glutLeaveMainLoop();
                break;
            }
        default:
            {
                break;
            }
    }
}

void GameApplication::keyboardReleaseControl(unsigned char key, int x, int y){
    switch (key)
    {
        /* moving around */
        case 'w': case 'W':
            {
                this->moveKeys[0] = false;
                break;
            }
        case 'a': case 'A':
            {
                this->moveKeys[1] = false;
                break;
            } 
        case 's': case 'S':
            {
                this->moveKeys[2] = false;
                break;
            }
        case 'd': case 'D':
            {
                this->moveKeys[3] = false;
                break;
            }
    }
}


void GameApplication::mouseEntryControl(int state){
    switch(state){
        case GLUT_LEFT:
        {
            this->mouseInside = false;
            if(this->mouseCaptured)
                this->initMouseLocation();
            break;
        }
        case GLUT_ENTERED:
        {
            this->mouseInside = true;
            break;
        }
    }
}


void GameApplication::mousePassiveMotionControl(int x, int y){
    if (this->mouseCaptured){
        int timePassed = glutGet(GLUT_ELAPSED_TIME) - lastMouseTimeMillis;
        
        if (timePassed >= 32){  // unnecassary to recalculate too frequently
            if(!mouseIsNew){
                int dist_x = x - lastMouseX;
                int dist_y = y - lastMouseY;

                this->rotateCamera(
                    ROTATION_DELTA * (GLfloat) dist_x / (GLfloat) timePassed,
                    -1.0 * ROTATION_DELTA * (GLfloat) dist_y / (GLfloat) timePassed);
                this->initMouseLocation();
            } else {
                this->recordMouseLocation(x, y);
                this->mouseIsNew = false;
            }
        }
    }
}


void GameApplication::captureMouse(){
    this->mouseCaptured = true;
    glutSetCursor(GLUT_CURSOR_NONE);
    //this->initMouseLocation();
}


void GameApplication::freeMouse(){
    this->mouseCaptured = false;
    glutSetCursor(GLUT_CURSOR_INHERIT);
}

/* Renders the current object on the focus. */
void GameApplication::render(){
    // The rendering function.
    // Any drawing should be done here
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if(this->terrain != nullptr){
        this->terrain->draw();
    }
    
    size_t objectsNum = this->objects.size();
    unsigned int i;
    GLObject * object = nullptr;
    for(i=0; i < objectsNum; i++){
        object = this->objects[i];
        object->draw();
    }

    Wall * wall = nullptr;
    size_t wallsNum = this->walls.size();
    for(i=0; i < wallsNum; i++){
        wall = this->walls[i];
        wall->draw();
    }

    glutSwapBuffers();
}

/* Readjust viewport on window size changes. Updates projection matrix. */
void GameApplication::resize(int width, int height){
    this->width = width;
    this->height = height;
    glViewport(0, 0, width, height);
    this->updateProjectionMatrix();
}

/* Arrow controls. */
void GameApplication::specialKeyControl(int key, int x, int y){
    switch (key){
        case GLUT_KEY_LEFT:
        {
            this->moveKeys[1] = true;
            break;
        }
        case GLUT_KEY_RIGHT:
        {
            this->moveKeys[3] = true;
            break;
        }
        case GLUT_KEY_DOWN:
        {
            this->moveKeys[2] = true;
            break;
        }
        case GLUT_KEY_UP:
        {
            this->moveKeys[0] = true;
            break;
        }
        default:
        {
            break;
        }
    }
}

void GameApplication::specialKeyReleaseControl(int key, int x, int y){
    switch (key){
        case GLUT_KEY_LEFT:
        {
            this->moveKeys[1] = false;
            break;
        }
        case GLUT_KEY_RIGHT:
        {
            this->moveKeys[3] = false;
            break;
        }
        case GLUT_KEY_DOWN:
        {
            this->moveKeys[2] = false;
            break;
        }
        case GLUT_KEY_UP:
        {
            this->moveKeys[0] = false;
            break;
        }
        default:
        {
            break;
        }
    }
}


void GameApplication::prepareCheckpoint(unsigned int checkpoint){
    this->checkpointDetector.clear();
    if(checkpoint < NUM_CHECKPOINTS){
        const point3& center = this->checkpoints[checkpoint];
        this->checkpointDetector.addStationary(
            AxisAlignedBoundingBox(
                center.x - wallWidthScale * 0.5, 0,          center.z - wallWidthScale,
                center.x + wallWidthScale * 0.5, wallHeight, center.z + wallWidthScale
            )
        );
    }
}

void GameApplication::checkpointArrival(){
    this->changeWallDiffuseTextures(this->checkpointTextures[this->nextCheckpoint][0]);
    this->changeWallSpecularTextures(this->checkpointTextures[this->nextCheckpoint][1]);
    this->lightingManager->enableLightSource(this->checkpointLights[this->nextCheckpoint]);
    if(this->checkpointObjects[this->nextCheckpoint] != nullptr)
        this->objects.push_back(this->checkpointObjects[this->nextCheckpoint]);
    
    this->prepareCheckpoint(++this->nextCheckpoint);
}


/* Wall generation */


void GameApplication::addHorizontalWallCollision(Wall * wall){
    const point3& center = wall->getPosition();
    const GLfloat& width = wall->getWidth();
    const GLfloat& heigth = wall->getHeight();

    this->collisionDetector.addStationary(
        AxisAlignedBoundingBox(
            center.x - width * 0.5, 0,      center.z - wallFilling,
            center.x + width * 0.5, heigth, center.z + wallFilling
        )
    );
}


void GameApplication::addVerticalWallCollision(Wall * wall){
    const point3& center = wall->getPosition();
    const GLfloat& width = wall->getWidth();
    const GLfloat& heigth = wall->getHeight();

    this->collisionDetector.addStationary(
        AxisAlignedBoundingBox(
            center.x - wallFilling * 0.5, 0,      center.z - width * 0.5,
            center.x + wallFilling * 0.5, heigth, center.z + width * 0.5 
        )
    );
}

void GameApplication::changeWallDiffuseTextures(unsigned int tex){
    wallTextures[0] = tex;

    size_t numWalls = this->walls.size();
    unsigned int i;
    for(i = 0; i < numWalls; i++){
        walls[i]->setDiffuseTexture(tex);
    }

}

void GameApplication::changeWallSpecularTextures(unsigned int tex){
    wallTextures[1] = tex;

    size_t numWalls = this->walls.size();
    unsigned int i;
    for(i = 0; i < numWalls; i++){
        walls[i]->setSpecularTexture(tex);
    }
}



void GameApplication::initWalls(){
    /* Outer walls */
    Wall * wall = new Wall3D(this->program, 10.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(0, wallYCenter, -5.0 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);

    wall = new Wall3D(this->program, 10.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(5.0 * wallWidthScale, wallYCenter, -10.0 * wallWidthScale));
    wall->rotateY(0.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);

    wall = new Wall3D(this->program, 10.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(10.0 * wallWidthScale, wallYCenter, -5.0 * wallWidthScale));
    wall->rotateY(-90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);

    wall = new Wall3D(this->program, 10.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(5.0 * wallWidthScale, wallYCenter, 0.0));
    wall->rotateY(180.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);


    /* Horizontal walls */


    /* row 1*/

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(0.5 * wallWidthScale, wallYCenter, -9.0 * wallWidthScale));
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);

    wall = new Wall3D(this->program, 2.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(9.0 * wallWidthScale, wallYCenter, -9.0 * wallWidthScale));
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);


    /* row 2 */

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(1.5 * wallWidthScale, wallYCenter, -8.0 * wallWidthScale));
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);

    wall = new Wall3D(this->program, 3.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(4.5 * wallWidthScale, wallYCenter, -8.0 * wallWidthScale));
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(7.5 * wallWidthScale, wallYCenter, -8.0 * wallWidthScale));
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(9.5 * wallWidthScale, wallYCenter, -8.0 * wallWidthScale));
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);


    /* row 3 */

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(0.5 * wallWidthScale, wallYCenter, -7.0 * wallWidthScale));
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(3.5 * wallWidthScale, wallYCenter, -7.0 * wallWidthScale));
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);

    wall = new Wall3D(this->program, 2.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(6.0 * wallWidthScale, wallYCenter, -7.0 * wallWidthScale));
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(8.5 * wallWidthScale, wallYCenter, -7.0 * wallWidthScale));
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);


    /* row 4 */

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(1.5 * wallWidthScale, wallYCenter, -6.0 * wallWidthScale));
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);

    wall = new Wall3D(this->program, 3.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(4.5 * wallWidthScale, wallYCenter, -6.0 * wallWidthScale));
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(9.5 * wallWidthScale, wallYCenter, -6.0 * wallWidthScale));
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);


    /* row 5 */

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(0.5 * wallWidthScale, wallYCenter, -5.0 * wallWidthScale));
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);

    wall = new Wall3D(this->program, 2.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(3.0 * wallWidthScale, wallYCenter, -5.0 * wallWidthScale));
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);

    wall = new Wall3D(this->program, 3.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(7.5 * wallWidthScale, wallYCenter, -5.0 * wallWidthScale));
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);


    /* row 6 */

    wall = new Wall3D(this->program, 2.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(2.0 * wallWidthScale, wallYCenter, -4.0 * wallWidthScale));
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);

    wall = new Wall3D(this->program, 2.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(6.0 * wallWidthScale, wallYCenter, -4.0 * wallWidthScale));
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);


    /* row 7 */

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(4.5 * wallWidthScale, wallYCenter, -3.0 * wallWidthScale));
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(7.5 * wallWidthScale, wallYCenter, -3.0 * wallWidthScale));
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(9.5 * wallWidthScale, wallYCenter, -3.0 * wallWidthScale));
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);


    /* row 8 */

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(0.5 * wallWidthScale, wallYCenter, -2.0 * wallWidthScale));
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(3.5 * wallWidthScale, wallYCenter, -2.0 * wallWidthScale));
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);

    wall = new Wall3D(this->program, 2.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(6.0 * wallWidthScale, wallYCenter, -2.0 * wallWidthScale));
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(8.5 * wallWidthScale, wallYCenter, -2.0 * wallWidthScale));
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);


    /* row 9 */

    wall = new Wall3D(this->program, 2.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(4.0 * wallWidthScale, wallYCenter, -1.0 * wallWidthScale));
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addHorizontalWallCollision(wall);


    /* Vertical walls */

    /* column 1 */

    wall = new Wall3D(this->program, 3.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(1.0 * wallWidthScale, wallYCenter, -2.5 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);

    
    /* column 2 */

    wall = new Wall3D(this->program, 3.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(2.0 * wallWidthScale, wallYCenter, -1.5 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);

    wall = new Wall3D(this->program, 4.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(2.0 * wallWidthScale, wallYCenter, -7.0 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);

    
    /* column 3 */

    wall = new Wall3D(this->program, 2.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(3.0 * wallWidthScale, wallYCenter, -3.0 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(3.0 * wallWidthScale, wallYCenter, -6.5 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(3.0 * wallWidthScale, wallYCenter, -9.5 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);

    
    /* column 4 */

    wall = new Wall3D(this->program, 3.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(4.0 * wallWidthScale, wallYCenter, -3.5 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(4.0 * wallWidthScale, wallYCenter, -8.5 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);

    
    /* column 5 */

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(5.0 * wallWidthScale, wallYCenter, -1.5 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(5.0 * wallWidthScale, wallYCenter, -4.5 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(5.0 * wallWidthScale, wallYCenter, -7.5 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(5.0 * wallWidthScale, wallYCenter, -9.5 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);


    /* column 6 */

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(6.0 * wallWidthScale, wallYCenter, -0.5 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);

    wall = new Wall3D(this->program, 2.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(6.0 * wallWidthScale, wallYCenter, -3.0 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(6.0 * wallWidthScale, wallYCenter, -5.5 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(6.0 * wallWidthScale, wallYCenter, -9.5 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);

    
    /* column 7 */

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(7.0 * wallWidthScale, wallYCenter, -1.5 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(7.0 * wallWidthScale, wallYCenter, -3.5 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);

    wall = new Wall3D(this->program, 2.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(7.0 * wallWidthScale, wallYCenter, -7.0 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(7.0 * wallWidthScale, wallYCenter, -9.5 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);


    /* column 8 */

    wall = new Wall3D(this->program, 3.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(8.0 * wallWidthScale, wallYCenter, -1.5 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);

    wall = new Wall3D(this->program, 3.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(8.0 * wallWidthScale, wallYCenter, -5.5 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(8.0 * wallWidthScale, wallYCenter, -8.5 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);


    /* column 9 */

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(9.0 * wallWidthScale, wallYCenter, -1.5 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(9.0 * wallWidthScale, wallYCenter, -3.5 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);

    wall = new Wall3D(this->program, 1.0 * wallWidthScale + wallFilling, wallHeight, wallFilling, wallTextureWidth, wallTextureHeight);
    wall->setPosition(point3(9.0 * wallWidthScale, wallYCenter, -7.5 * wallWidthScale));
    wall->rotateY(90.0);
    wall->setDiffuseTexture(wallTextures[0]);
    wall->setSpecularTexture(wallTextures[1]);
    this->walls.push_back(wall);
    this->addVerticalWallCollision(wall);
}