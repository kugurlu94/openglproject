#ifndef __WALL_H
#define __WALL_H

#include <Angel.h>
#include "obj.h"

class Wall : public GLObject {
    protected:
        GLfloat width;
        GLfloat height;

    public:
        explicit Wall(GLuint program, const GLfloat& width = 1.0, const GLfloat& height = 1.0);

        GLfloat getWidth() const;
        GLfloat getHeight() const;
};


class Wall2D : public Wall {
    protected:
        GLuint vao_id;
        GLuint vbo_ids[2];

        GLfloat textureWidth;
        GLfloat textureHeight;

    public:
        explicit Wall2D(
            GLuint program, const GLfloat& width = 1.0, const GLfloat& height = 1.0,
            const GLfloat& textureWidth = 1.0, const GLfloat textureHeight = 1.0);
        virtual void allocate();
        virtual void draw();

};

class Wall3D : public Wall {
    protected:
        GLuint vao_id;
        GLuint vbo_ids[2];

        GLfloat thickness;
        GLfloat textureWidth;
        GLfloat textureHeight;

    public:
        explicit Wall3D(
            GLuint program, const GLfloat& width = 1.0, const GLfloat& height = 1.0, const GLfloat& thickness = 0.1,
            const GLfloat& textureWidth = 1.0, const GLfloat textureHeight = 1.0);
        virtual void allocate();
        virtual void draw();
};

#endif