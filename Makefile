CC = g++
CPPVERSION = -std=c++11
DEBUG = -O2 -g
LDLIBS = -lglut -lGLEW -lGL -lGLU -lpthread -lSOIL

CXXINCS = -I include
COMMON_DIR = common

INIT_SHADER = $(COMMON_DIR)/InitShader.cpp
INIT_SHADER_OBJ = $(COMMON_DIR)/InitShader.o

ABSAPP_H = absapp.h
ABSAPP = absapp.cpp
ABSAPP_OBJ = absapp.o

APP_H = app.h
APP = app.cpp
APP_OBJ = app.o

OBJ_H = obj.h
OBJ = obj.cpp
OBJ_OBJ = obj.o

CAMERA_H = camera.h
CAMERA = camera.cpp
CAMERA_OBJ = camera.o

LIGHT_H = light.h
LIGHT = light.cpp
LIGHT_OBJ = light.o

MATERIAL_H = material.h
MATERIAL = material.cpp
MATERIAL_OBJ = material.o

TERRAIN_H = terrain.h
TERRAIN = terrain.cpp
TERRAIN_OBJ = terrain.o

TEXTURE_H = texture.h
TEXTURE = texture.cpp
TEXTURE_OBJ = texture.o

WALL_H = wall.h
WALL = wall.cpp
WALL_OBJ = wall.o

COLLISION_H = collision.h
COLLISION = collision.cpp
COLLISION_OBJ = collision.o

UTILS_H = utils.h
UTILS = utils.cpp
UTILS_OBJ = utils.o


MAIN_CPP = main.cpp

TARGETS = main.out

all: $(TARGETS)

.PHONY: clean cleanall

$(TARGETS): $(UTILS_OBJ) $(COLLISION_OBJ) $(WALL_OBJ) $(TEXTURE_OBJ) $(TERRAIN_OBJ) $(MATERIAL_OBJ) $(LIGHT_OBJ) $(CAMERA_OBJ) $(OBJ_OBJ) $(APP_OBJ) $(ABSAPP_OBJ) $(INIT_SHADER_OBJ) $(MAIN_CPP)
	$(CC) $(CPPVERSION) $(DEBUG) $(CXXINCS) $(INIT_SHADER_OBJ) $(ABSAPP_OBJ) $(APP_OBJ) $(OBJ_OBJ) $(CAMERA_OBJ) $(LIGHT_OBJ) $(MATERIAL_OBJ) $(TERRAIN_OBJ) $(TEXTURE_OBJ) $(WALL_OBJ) $(COLLISION_OBJ) $(UTILS_OBJ) $(MAIN_CPP) $(LDLIBS) -o $@

$(UTILS_OBJ): $(UTILS_H) $(UTILS)
	$(CC) $(CPPVERSION) -c -Wall $(CXXINCS) $(UTILS) -o $(UTILS_OBJ)

$(COLLISION_OBJ): $(COLLISION_H) $(COLLISION)
	$(CC) $(CPPVERSION) -c -Wall $(CXXINCS) $(COLLISION) -o $(COLLISION_OBJ)

$(WALL_OBJ): $(WALL_H) $(WALL)
	$(CC) $(CPPVERSION) -c -Wall $(CXXINCS) $(WALL) -o $(WALL_OBJ)

$(TEXTURE_OBJ): $(TEXTURE_H) $(TEXTURE)
	$(CC) $(CPPVERSION) -c -Wall $(CXXINCS) $(TEXTURE) -o $(TEXTURE_OBJ)

$(TERRAIN_OBJ): $(TERRAIN_H) $(TERRAIN)
	$(CC) $(CPPVERSION) -c -Wall $(CXXINCS) $(TERRAIN) -o $(TERRAIN_OBJ)

$(LIGHT_OBJ): $(LIGHT_H) $(LIGHT)
	$(CC) $(CPPVERSION) -c -Wall $(CXXINCS) $(LIGHT) -o $(LIGHT_OBJ)

$(MATERIAL_OBJ): $(MATERIAL_H) $(MATERIAL)
	$(CC) $(CPPVERSION) -c -Wall $(CXXINCS) $(MATERIAL) -o $(MATERIAL_OBJ)

$(CAMERA_OBJ): $(CAMERA_H) $(CAMERA)
	$(CC) $(CPPVERSION) -c -Wall $(CXXINCS) $(CAMERA) -o $(CAMERA_OBJ)

$(OBJ_OBJ): $(OBJ_H) $(OBJ)
	$(CC) $(CPPVERSION) -c -Wall $(CXXINCS) $(OBJ) -o $(OBJ_OBJ)

$(APP_OBJ): $(APP_H) $(APP)
	$(CC) $(CPPVERSION) -c -Wall $(CXXINCS) $(APP) -o $(APP_OBJ)

$(ABSAPP_OBJ): $(ABSAPP_H) $(ABSAPP) $(INIT_SHADER_OBJ)
	$(CC) $(CPPVERSION) -c -Wall $(CXXINCS) $(ABSAPP) -o $(ABSAPP_OBJ)

$(INIT_SHADER_OBJ): $(INIT_SHADER)
	$(CC) $(CPPVERSION) -c -Wall $(INIT_SHADER) -o $(INIT_SHADER_OBJ)

clean:
	\rm $(TARGETS)

cleanall:
	\rm $(INIT_SHADER_OBJ) $(ABSAPP_OBJ) $(APP_OBJ) $(OBJ_OBJ) $(CAMERA_OBJ) $(LIGHT_OBJ) $(TERRAIN_OBJ) $(TEXTURE_OBJ) $(UTILS_OBJ) $(TARGETS)
