#version 450

const uint MAX_LIGHT_SOURCES = 5;

in  vec4 vPosition;
in  vec3 vNormal;
in  vec2 vTexCoord;

out vec2 texCoord;
out vec4 shadedColor;

/* phong shading attributes */
out vec3 fragNormal;
out vec3 fragPosition;

uniform uint DrawMode; // Draw mode 0 for wireframe, 1 for shading, 2 for texture, 3 for both texture and shading
uniform vec4 Color;
uniform mat4 ModelMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;


void main() 
{
    // Transform vertex  position into eye coordinates
    vec4 worldCoord = ModelMatrix * vPosition;

    if (DrawMode == 1 || DrawMode == 3){ // Shading mode
        // phong shading
        fragNormal = (ModelMatrix * vec4(vNormal, 0.0)).xyz; // Normals
        fragPosition = worldCoord.xyz; // Position
    }

    shadedColor = Color; // for wireframe and textureless fragments.
    texCoord = vTexCoord;
    gl_Position = ProjectionMatrix * ViewMatrix * worldCoord;
    
}
