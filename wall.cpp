#include "wall.h"


Wall::Wall(GLuint program, const GLfloat& width, const GLfloat& height)
	:GLObject(program), width(width), height(height){}

GLfloat Wall::getWidth() const{
	return this->width;
}

GLfloat Wall::getHeight() const{
	return this->height;
}


Wall2D::Wall2D(GLuint program, const GLfloat& width, const GLfloat& height, const GLfloat& textureWidth, const GLfloat textureHeight)
	:Wall(program, width, height), textureWidth(textureWidth), textureHeight(textureHeight){
		this->vao_id = 0;
		this->vbo_ids[0] = 0;
		this->vbo_ids[1] = 0;
	}

void Wall2D::allocate(){
    glUseProgram(this->program);
    // Create an initilize vertex array object
    if(this->vao_id == 0)
        glGenVertexArrays(1, &(this->vao_id));

    glBindVertexArray(this->vao_id);
    // Create and initialize a buffer object;
    // First index for vertex objects, second for index objects.
    if(this->vbo_ids[0] == 0 && this->vbo_ids[1] == 0)
        glGenBuffers(2, &vbo_ids[0]);

    GLfloat horizontalDelta = this->width * 0.5;
    GLfloat verticalDelta = this->height * 0.5;

    GLfloat horizontalTexCord = this->width / this->textureWidth;
    GLfloat verticalTexCord = this->height / this->textureHeight;

    point4 vertices[4] = {
        point4(-horizontalDelta, -verticalDelta,  0.0, 1),
        point4(-horizontalDelta,  verticalDelta,  0.0, 1),
        point4( horizontalDelta,  verticalDelta,  0.0, 1),
        point4( horizontalDelta, -verticalDelta,  0.0, 1)
    };

    vec3 normals[4] = {
        vec3(0.0, 0.0, 1.0),
        vec3(0.0, 0.0, 1.0),
        vec3(0.0, 0.0, 1.0),
        vec3(0.0, 0.0, 1.0)
    };

    vec2 texCoords[4] = { // take the advangate of texture repeat
        vec2(0.0, 0.0),
        vec2(0.0, verticalTexCord),
        vec2(horizontalTexCord, verticalTexCord),
        vec2(horizontalTexCord, 0.0)
    };

    GLuint indices[6] = {0,2,1, 0,3,2};

    size_t size_vertices = sizeof(vertices);
    size_t size_texCoords = sizeof(texCoords);
    size_t size_normals = sizeof(normals);

    glBindBuffer(GL_ARRAY_BUFFER, vbo_ids[0]);
    glBufferData(GL_ARRAY_BUFFER, size_vertices + size_texCoords + size_normals, NULL, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, size_vertices, vertices);
    glBufferSubData(GL_ARRAY_BUFFER, size_vertices, size_texCoords, texCoords);
    glBufferSubData(GL_ARRAY_BUFFER, size_vertices + size_texCoords, size_normals, normals);

    // set up vertex attributes;
    glEnableVertexAttribArray(this->positionAttrib);
    glVertexAttribPointer(this->positionAttrib, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

    glEnableVertexAttribArray(this->texCoordAttrib);
    glVertexAttribPointer(this->texCoordAttrib, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(size_vertices));

    glEnableVertexAttribArray(this->normalAttrib);
    glVertexAttribPointer(this->normalAttrib, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(size_vertices + size_texCoords));


    // set up index buffer object.
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_ids[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    glBindVertexArray(0);
    glUseProgram(0);

    this->allocated = true;
}

void Wall2D::draw(){
    if(!this->isAllocated()) // lazy allocation
        this->allocate();

    this->loadModelMatrix();
    this->loadMaterial();

    glUseProgram(this->program);
    glBindVertexArray(this->vao_id);

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)0);

    glBindVertexArray(0);
    glUseProgram(0);
}

Wall3D::Wall3D(GLuint program, const GLfloat& width, const GLfloat& height, const GLfloat& thickness, const GLfloat& textureWidth, const GLfloat textureHeight)
	:Wall(program, width, height), thickness(thickness), textureWidth(textureWidth), textureHeight(textureHeight){
		this->vao_id = 0;
		this->vbo_ids[0] = 0;
		this->vbo_ids[1] = 0;
	}

void Wall3D::allocate(){
    glUseProgram(this->program);
    // Create an initilize vertex array object
    if(this->vao_id == 0)
        glGenVertexArrays(1, &(this->vao_id));

    glBindVertexArray(this->vao_id);
    // Create and initialize a buffer object;
    // First index for vertex objects, second for index objects.
    if(this->vbo_ids[0] == 0 && this->vbo_ids[1] == 0)
        glGenBuffers(2, &vbo_ids[0]);

    GLfloat horizontalDelta = this->width * 0.5;
    GLfloat verticalDelta = this->height * 0.5;
    GLfloat depthDelta = this->thickness * 0.5;

    GLfloat horizontalTexCord = this->width / this->textureWidth;
    GLfloat verticalTexCord = this->height / this->textureHeight;

    point4 vertices[16] = {
        point4(-horizontalDelta, -verticalDelta,  depthDelta, 1.0),
        point4(-horizontalDelta,  verticalDelta,  depthDelta, 1.0),
        point4( horizontalDelta,  verticalDelta,  depthDelta, 1.0),
        point4( horizontalDelta, -verticalDelta,  depthDelta, 1.0),
        point4(-horizontalDelta, -verticalDelta, -depthDelta, 1.0),
        point4(-horizontalDelta,  verticalDelta, -depthDelta, 1.0),
        point4( horizontalDelta,  verticalDelta, -depthDelta, 1.0),
        point4( horizontalDelta, -verticalDelta, -depthDelta, 1.0),

        point4(-horizontalDelta, -verticalDelta,  depthDelta, 1.0),
        point4(-horizontalDelta,  verticalDelta,  depthDelta, 1.0),
        point4(-horizontalDelta,  verticalDelta, -depthDelta, 1.0),
        point4(-horizontalDelta, -verticalDelta, -depthDelta, 1.0),
		point4( horizontalDelta, -verticalDelta,  depthDelta, 1.0),
        point4( horizontalDelta,  verticalDelta,  depthDelta, 1.0),
       	point4( horizontalDelta,  verticalDelta, -depthDelta, 1.0),
        point4( horizontalDelta, -verticalDelta, -depthDelta, 1.0)

    };

    vec3 normals[16] = {
        vec3(0.0, 0.0, 1.0),
        vec3(0.0, 0.0, 1.0),
        vec3(0.0, 0.0, 1.0),
        vec3(0.0, 0.0, 1.0),
        vec3(0.0, 0.0, -1.0),
        vec3(0.0, 0.0, -1.0),
        vec3(0.0, 0.0, -1.0),
        vec3(0.0, 0.0, -1.0),

        vec3(-1.0, 0.0, 0.0),
        vec3(-1.0, 0.0, 0.0),
        vec3(-1.0, 0.0, 0.0),
        vec3(-1.0, 0.0, 0.0),
        vec3(1.0, 0.0, 0.0),
        vec3(1.0, 0.0, 0.0),
        vec3(1.0, 0.0, 0.0),
        vec3(1.0, 0.0, 0.0)
    };

    vec2 texCoords[16] = { // take the advangate of texture repeat
        vec2(0.0, 0.0),
        vec2(0.0, verticalTexCord),
        vec2(horizontalTexCord, verticalTexCord),
        vec2(horizontalTexCord, 0.0),
        vec2(0.0, 0.0),
        vec2(0.0, verticalTexCord),
        vec2(horizontalTexCord, verticalTexCord),
        vec2(horizontalTexCord, 0.0),

        vec2(0.0, 0.0),
        vec2(0.0, verticalTexCord),
        vec2(horizontalTexCord * this->thickness / this->width, verticalTexCord),
        vec2(horizontalTexCord * this->thickness / this->width, 0.0),
        vec2(0.0, 0.0),
        vec2(0.0, verticalTexCord),
        vec2(horizontalTexCord * this->thickness / this->width, verticalTexCord),
        vec2(horizontalTexCord * this->thickness / this->width, 0.0)
    };

    GLuint indices[24] = {
    	0,2,1, 0,3,2, 4,6,5, 4,7,6,
    	8,10,9, 8,11,10, 12,14,13, 12,15,14
    };

    size_t size_vertices = sizeof(vertices);
    size_t size_texCoords = sizeof(texCoords);
    size_t size_normals = sizeof(normals);

    glBindBuffer(GL_ARRAY_BUFFER, vbo_ids[0]);
    glBufferData(GL_ARRAY_BUFFER, size_vertices + size_texCoords + size_normals, NULL, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, size_vertices, vertices);
    glBufferSubData(GL_ARRAY_BUFFER, size_vertices, size_texCoords, texCoords);
    glBufferSubData(GL_ARRAY_BUFFER, size_vertices + size_texCoords, size_normals, normals);

    // set up vertex attributes;
    glEnableVertexAttribArray(this->positionAttrib);
    glVertexAttribPointer(this->positionAttrib, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

    glEnableVertexAttribArray(this->texCoordAttrib);
    glVertexAttribPointer(this->texCoordAttrib, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(size_vertices));

    glEnableVertexAttribArray(this->normalAttrib);
    glVertexAttribPointer(this->normalAttrib, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(size_vertices + size_texCoords));


    // set up index buffer object.
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_ids[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    glBindVertexArray(0);
    glUseProgram(0);

    this->allocated = true;
}


void Wall3D::draw(){
    if(!this->isAllocated()) // lazy allocation
        this->allocate();

    this->loadModelMatrix();
    this->loadMaterial();

    glUseProgram(this->program);
    glBindVertexArray(this->vao_id);

    glDrawElements(GL_TRIANGLES, 24, GL_UNSIGNED_INT, (GLvoid*)0);

    glBindVertexArray(0);
    glUseProgram(0);
}