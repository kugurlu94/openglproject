#ifndef __CAMERA_H
#define __CAMERA_H

#include <Angel.h>

typedef Angel::vec4  point4;
typedef Angel::vec4  vec4;
typedef Angel::vec3  vec3;


class Camera {
    protected:
        static const GLfloat DEFAULT_YAW;
        static const GLfloat DEFAULT_PITCH;
        static const GLfloat MAX_PITCH;
        static const GLfloat MIN_PITCH;

        /* Camera position */
        point4 position;

        /* Camera yaw and pitch, used for deriving front direction vector*/
        GLfloat yaw;
        GLfloat pitch;

        /* direction vector */
        vec4 direction;

        /* up vector */
        vec4 up;

        /* View Contained view matrix */
        mat4 viewMatrix;

        /* Whether or not view matrix is calculated */
        bool viewFresh;

        /* Wheter or not up and direction vectors are fresh */
        bool vectorsFresh;

        /* Updates view matrix */
        void updateViewMatrix();

        /* Updates vectors */
        void updateVectors();

    public:
        explicit Camera(const point4& position, const GLfloat& yaw = DEFAULT_YAW, const GLfloat& pitch = DEFAULT_PITCH);
        explicit Camera(const GLfloat& eye_x, const GLfloat& eye_y, const GLfloat& eye_z, const GLfloat& yaw = DEFAULT_YAW, const GLfloat& pitch = DEFAULT_PITCH);
        explicit Camera();

        /* Methods to move camera around */
        void moveX(const GLfloat& delta_x);
        void moveY(const GLfloat& delta_y);
        void moveZ(const GLfloat& delta_z);
        void move(const vec3& move_vec);

        /* Methods to rotate camera */
        void yawCamera(const GLfloat& delta_yaw);
        void pitchCamera(const GLfloat& delta_pitch);

        /* Method to obtain direction */
        const vec4& getDirection();

        /* Method to obtain position */
        const point4& getPosition();

        /* Method to obtain view matrix */
        const mat4& getViewMatrix();

};

#endif