#ifndef __ABSAPP_H
#define __ABSAPP_H

#include <string>
#include <Angel.h>

typedef Angel::vec4 color4;

/* 
    Abstract Application class the user should subclass this class
    and add application specific capabilites.
*/
class Application {
    private:
        /* Callback methods to pass to glut callbacks */
        friend void idleCallback();
        friend void displayCallback();
        friend void overlayDisplayCallback();
        friend void reshapeCallback(int width, int height);
        friend void keyboardCallback(unsigned char key, int x, int y);
        friend void keyboardUpCallback(unsigned char key, int x, int y);
        friend void mouseCallback(int button, int state, int x, int y);
        friend void motionCallback(int x, int y);
        friend void passiveMotionCallback(int x, int y);
        friend void visibilityCallback(int state);
        friend void entryCallback(int state);
        friend void specialCallBack(int key, int x, int y);
        friend void specialUpCallBack(int key, int x, int y);

    protected:
        /* Singleton application pointer */
        static Application * currentApplication;
        
        /* Shader program related static variables. */
        static const char* VERTEX_SHADER;
        static const char* FRAGMENT_SHADER;
        static const char* VIEW_MATRIX_NAME;
        static const char* PROJECTION_MATRIX_NAME;

        /* Window related */
        std::string windowTitle;
        int width, height;
        int windowHandle;

        /* program id*/
        GLuint program;

        /* uniform variable locations */
        GLuint modelMatrixLocation;
        GLuint viewMatrixLocation;
        GLuint projectionMatrixLocation;

        /* trasformation matrices */
        mat4 viewMatrix;  // World Coordinates to view coordinates.
        mat4 projectionMatrix;  // Projection.

        /* Set initial opengl configurations */
        virtual void glConf();
        void setClearColor(const color4& color);

        /* Methods to update matrices */
        virtual void updateViewMatrix() =0;
        virtual void updateProjectionMatrix() =0;

        /* 
            general utility methods that attaches to callbacks
            Override these methods to specify callback behaviours.
        */
        virtual void idle(){};
        virtual void render(){};
        virtual void renderOverlay(){};
        virtual void resize(int width, int height){};
        virtual void keyboardControl(unsigned char key, int x, int y){};
        virtual void keyboardReleaseControl(unsigned char key, int x, int y){};
        virtual void mouseControl(int button, int state, int x, int y){};
        virtual void mouseMotionControl(int x, int y){};
        virtual void mousePassiveMotionControl(int x, int y){};
        virtual void windowVisibilityControl(int state){};
        virtual void mouseEntryControl(int state){};
        virtual void specialKeyControl(int key, int x, int y){};
        virtual void specialKeyReleaseControl(int key, int x, int y){};

        /* Attach & Detach callbacks */
        virtual void activateIdle();
        virtual void deactivateIdle();
        virtual void activateRender();
        virtual void deactivateRender();
        virtual void activateRenderOverlay();
        virtual void deactivateRenderOverlay();
        virtual void activateResize();
        virtual void deactivateResize();
        virtual void activateKeyboardControl();
        virtual void deactivateKeyboardControl();
        virtual void activateKeyboardReleaseControl();
        virtual void deactivateKeyboardReleaseControl();
        virtual void activateMouseControl();
        virtual void deactivateMouseControl();
        virtual void activateMouseMotionControl();
        virtual void deactivateMouseMotionControl();
        virtual void activateMousePassiveMotionControl();
        virtual void deactivateMousePassiveMotionControl();
        virtual void activateWindowVisibilityControl();
        virtual void deactivateWindowVisibilityControl();
        virtual void activateMouseEntryControl();
        virtual void deactivateMouseEntryControl();
        virtual void activateSpecialKeyControl();
        virtual void deactivateSpecialKeyControl();
        virtual void activateSpecialKeyReleaseControl();
        virtual void deactivateSpecialKeyReleaseControl();

    public:
        explicit Application(const std::string& title, const int w_width, const int w_height);
        virtual void init(int argc, char ** argv);
        virtual void start();
};
#endif
