#ifndef __OBJ_H
#define __OBJ_H

#include <Angel.h>
#include <vector>
#include "light.h"
#include "material.h"
#include "texture.h"
#include "utils.h"


typedef Angel::vec4  color4;
typedef Angel::vec4  point4;
typedef Angel::vec3  point3;
typedef Angel::vec2  point2;

// Class that describes a objects state.
// Genereates the model matrix.
class GLObjectState {
    protected:
        // Initial state variables for recovery.
        GLfloat init_x;
        GLfloat init_y;
        GLfloat init_z;
        GLfloat init_scale;

        // State variables to generate ModelViewMatrix
        mat4 rotateMatrix;
        GLfloat m_scale;
        vec3 position;

        // Model matrix
        mat4 modelMatrix;

        // whether or not model matrix is fresh
        bool modelMatrixFresh;

        void updateModelMatrix();

    public:
        static const GLfloat DEFAULT_X;
        static const GLfloat DEFAULT_Y;
        static const GLfloat DEFAULT_Z;
        static const GLfloat DEFAULT_SCALE;
        static const point3 DEFAULT_POSITION;

        // Explicit constructor.
        explicit GLObjectState(
            const GLfloat& x_rot = DEFAULT_X,
            const GLfloat& y_rot = DEFAULT_Y, 
            const GLfloat& z_rot = DEFAULT_Z,
            const GLfloat& scale = DEFAULT_SCALE,
            const point3& position = DEFAULT_POSITION);
        
        // rotation methods
        void rotateX(const GLfloat& rot);
        void rotateY(const GLfloat& rot);
        void rotateZ(const GLfloat& rot);

        // scale methods
        void scale(const GLfloat& val);
        void scaleIn(const GLfloat& val);
        void scaleOut(const GLfloat& val);

        // move methods
        void move(const point3& move_val);
        void setPosition(const point3& newPosition);

        // reset methods
        void resetRotation();
        void reset(); // reset doesn't reset position.

        const point3& getPosition() const;
        const mat4& getModelMatrix();
};

/* Abstract base class, covering anything that is drawable */
class Drawable {
    public:
        virtual void draw() =0; // method for drawing
};


// abstract GLObject used for abstracting the details of
// each type of object to draw
class GLObject : public Drawable {
    protected:
        static const char* MODEL_MATRIX_NAME; // Model matrix name
        static const char* VERTEX_POSITION_ATTRIB_NAME; // Vertex position attribute name
        static const char* TEXTURE_COORDINATE_ATTRIB_NAME; // texture coordinate attribute name
        static const char* NORMAL_POSITION_ATTRIB_NAME; // Normal attribute name.

        GLObjectState objectState; // object state.

        GLuint program; // program object belongs to.
        GLuint modelMatrixLocation; // model matrix attrib location.
        GLuint positionAttrib; // position attrib location.
        GLuint texCoordAttrib; // texture attrib location.
        GLuint normalAttrib; // normal attrib location.
        
        /* Material attrib locations */
        GLuint materialDiffuseActiveLocation;
        GLuint materialDiffuseLocation;
        GLuint materialSpecularActiveLocation;
        GLuint materialSpecularLocation;
        GLuint materialShininessLocation;

        /* texture activa location */
        GLuint textureActiveLocation;

        bool allocated; // whether or not obj is allocated on gpu.

        MaterialProperties materialProperties;

        void setMaterialLocations();
        void loadMaterial();

        void loadModelMatrix(const mat4& modelMatrix);
        virtual void loadModelMatrix(); // loads model matrix uniform variable.
    public:
        explicit GLObject(GLuint program);

        virtual bool isAllocated();
        virtual void allocate(){}; // method for allocating memory in gpu
        virtual void deallocate(){}; // method for deallocating memory in gpu
        virtual void draw() =0; // method for drawing the pre-allocated object

        // texture
        virtual void setDiffuseTexture(const unsigned int& newTexture);
        virtual void setSpecularTexture(const unsigned int& newTexture);

        // matrial properties
        virtual void setMaterialProperties(const MaterialProperties& m_properties);
        virtual void setShininess(const GLfloat& shininess);

        // rotation methods
        virtual void rotateX(const GLfloat& rot);
        virtual void rotateY(const GLfloat& rot);
        virtual void rotateZ(const GLfloat& rot);

        // scale method
        virtual void scale(const GLfloat& val);
        
        // move methods
        virtual void move(const point3& move_val);
        virtual void setPosition(const point3& newPosition);

        virtual const point3& getPosition() const;
        virtual const mat4& getModelMatrix();
};

// Simple cube class for ease of testing.
class GLCube : public GLObject {
    protected:
        GLuint vao_id; // vertex array object for storing vertex data.
        GLuint vbo_ids[2]; // vertex buffer objects. index 0 for vertex data, index 1 for index data.

        // Vertices
        point4 vertices[8] = {
            point4(-.5f, -.5f,  .5f, 1),
            point4(-.5f,  .5f,  .5f, 1),
            point4( .5f,  .5f,  .5f, 1),
            point4( .5f, -.5f,  .5f, 1),
            point4(-.5f, -.5f, -.5f, 1),
            point4(-.5f,  .5f, -.5f, 1),
            point4( .5f,  .5f, -.5f, 1),
            point4( .5f, -.5f, -.5f, 1),
        };

        // normal vectors
        vec3 normals[8] = {
            normalize(vec3(-.5f, -.5f,  .5f)),
            normalize(vec3(-.5f,  .5f,  .5f)),
            normalize(vec3( .5f,  .5f,  .5f)),
            normalize(vec3( .5f, -.5f,  .5f)),
            normalize(vec3(-.5f, -.5f, -.5f)),
            normalize(vec3(-.5f,  .5f, -.5f)),
            normalize(vec3( .5f,  .5f, -.5f)),
            normalize(vec3( .5f, -.5f, -.5f)),
        };

        vec2 texCoords[8] = {
            vec2(0.0, 0.0),
            vec2(0.0, 1.0),
            vec2(1.0, 1.0),
            vec2(1.0, 0.0),
            vec2(1.0, 1.0),
            vec2(1.0, 2.0),
            vec2(2.0, 2.0),
            vec2(2.0, 1.0)
        };

        GLuint indices[36] = {
            0,2,1,  0,3,2,
            4,3,0,  4,7,3,
            4,1,5,  4,0,1,
            3,6,2,  3,7,6,
            1,6,5,  1,2,6,
            7,5,6,  7,4,5
        };

    public:
        explicit GLCube(GLuint program);
        virtual void allocate();
        virtual void draw();

        void allocateNormals();
};


// Object which reads the vertex data from a OFF file.
class GLOffObject : public GLObject {
    protected:
        GLuint vao_id; // vertex array object for storing vertex data.
        GLuint vbo_ids[2]; // vertex buffer objects. index 0 for vertex data, index 1 for index data.

        int numVertices; // number of vertices.
        point4* vertices;
        int numIndices; // number of indices.
        GLuint* indices;
    public:
        explicit GLOffObject(GLuint program, std::string filename);
        virtual void allocate();
        virtual void draw();
};

// Object which reads the vertex data from a OFFX file.
class GLOffXObject : public GLObject {
    protected:
        GLuint vao_id; // vertex array object for storing vertex data.
        GLuint vbo_ids[2]; // vertex buffer objects. index 0 for vertex data, index 1 for normal data and index 2 for index data.

        int numVertices; // number of vertices.
        point4* vertices;
        
        int numTexCoords; // number of texture coordinates
        point2* texCoords;

        int numNormals; // number of normals.
        point3* normals;

        int numIndices; // number of indices.
        GLuint* indices;

        void parseOffx(std::string filename);
    public:
        explicit GLOffXObject(GLuint program, std::string filename);
        virtual void allocate();
        virtual void draw();
};

#endif