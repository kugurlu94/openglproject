#include "terrain.h"


TerrainUnit::TerrainUnit(GLuint program, const GLfloat& size): GLObject(program), size(size){
    this->vao_id = 0;
    this->vbo_ids[0] = 0;
    this->vbo_ids[1] = 0;
}

GLfloat TerrainUnit::getSize() const{
    return this->size;
}


SimpleTerrainUnit::SimpleTerrainUnit(GLuint program, const GLfloat& size, const GLfloat& textureSize):
    TerrainUnit(program, size), textureSize(textureSize)
{
    this->objectState.rotateX(-90.0); // horizontal
}


void SimpleTerrainUnit::allocate(){
    glUseProgram(this->program);
    // Create an initilize vertex array object
    if(this->vao_id == 0)
        glGenVertexArrays(1, &(this->vao_id));

    glBindVertexArray(this->vao_id);
    // Create and initialize a buffer object;
    // First index for vertex objects, second for index objects.
    if(this->vbo_ids[0] == 0 && this->vbo_ids[1] == 0)
        glGenBuffers(2, &vbo_ids[0]);

    GLfloat edgeDelta = this->size * 0.5;
    GLfloat texCord = this->size / this->textureSize;

    point4 vertices[4] = {
        point4(-edgeDelta, -edgeDelta,  0.0, 1),
        point4(-edgeDelta,  edgeDelta,  0.0, 1),
        point4( edgeDelta,  edgeDelta,  0.0, 1),
        point4( edgeDelta, -edgeDelta,  0.0, 1)
    };

    vec3 normals[4] = {
        vec3(0.0, 0.0, 1.0),
        vec3(0.0, 0.0, 1.0),
        vec3(0.0, 0.0, 1.0),
        vec3(0.0, 0.0, 1.0)
    };

    vec2 texCoords[4] = { // take the advangate of texture repeat
        vec2(0.0, 0.0),
        vec2(0.0, texCord),
        vec2(texCord, texCord),
        vec2(texCord, 0.0)
    };

    GLuint indices[6] = {0,2,1, 0,3,2};

    size_t size_vertices = sizeof(vertices);
    size_t size_texCoords = sizeof(texCoords);
    size_t size_normals = sizeof(normals);

    glBindBuffer(GL_ARRAY_BUFFER, vbo_ids[0]);
    glBufferData(GL_ARRAY_BUFFER, size_vertices + size_texCoords + size_normals, NULL, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, size_vertices, vertices);
    glBufferSubData(GL_ARRAY_BUFFER, size_vertices, size_texCoords, texCoords);
    glBufferSubData(GL_ARRAY_BUFFER, size_vertices + size_texCoords, size_normals, normals);

    // set up vertex attributes;
    glEnableVertexAttribArray(this->positionAttrib);
    glVertexAttribPointer(this->positionAttrib, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

    glEnableVertexAttribArray(this->texCoordAttrib);
    glVertexAttribPointer(this->texCoordAttrib, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(size_vertices));

    glEnableVertexAttribArray(this->normalAttrib);
    glVertexAttribPointer(this->normalAttrib, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(size_vertices + size_texCoords));


    // set up index buffer object.
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_ids[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    glBindVertexArray(0);
    glUseProgram(0);

    this->allocated = true;
}


void SimpleTerrainUnit::draw(){
    if(!this->isAllocated()) // lazy allocation
        this->allocate();

    this->loadModelMatrix();
    this->loadMaterial();

    glUseProgram(program);
    glBindVertexArray(vao_id);

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (GLvoid*)0);

    glBindVertexArray(0);
    glUseProgram(0);
}


SimpleTerrain::SimpleTerrain(TerrainUnit * terrainUnit, const unsigned int& x_num, const unsigned int& y_num, const point3& center):
    Terrain(),
    terrainUnit(terrainUnit),
    x_num(x_num),
    y_num(y_num),
    center(center){};


void SimpleTerrain::draw(){
    if (this->terrainUnit != nullptr){
        GLfloat unitSize = this->terrainUnit->getSize();
        
        const GLfloat translationStartX = center.x - 0.5 * unitSize * (this->x_num - 1);
        const GLfloat translationStartY = center.z + 0.5 * unitSize * (this->y_num - 1);

        // Loop trough each TerrainUnit and translate them the proper amount.
        unsigned int x, y;
        for(x = 0; x < this->x_num; x++){
            for(y = 0; y < this->y_num; y++){
                this->terrainUnit->setPosition(point3(translationStartX + unitSize*x, center.y, translationStartY - unitSize*y));
                this->terrainUnit->draw();
            }
        }
    }

}
