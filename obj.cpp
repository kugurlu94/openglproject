#include "obj.h"


/*
    GLObjectState class implementation.
    Class is used for storing the rotation and scale information
    of an object. And used for generating the ModelView matrix for
    object in its current state.   
*/
const GLfloat GLObjectState::DEFAULT_X = 0.0;
const GLfloat GLObjectState::DEFAULT_Y = 0.0;
const GLfloat GLObjectState::DEFAULT_Z = 0.0;
const GLfloat GLObjectState::DEFAULT_SCALE = 1.0;
const point3 GLObjectState::DEFAULT_POSITION = point3(0.0, 0.0, 0.0);


// Custom constructor.
GLObjectState::GLObjectState(const GLfloat& x_rot, const GLfloat& y_rot, const GLfloat& z_rot, const GLfloat& scale, const point3& position):
    init_x(x_rot),
    init_y(y_rot),
    init_z(z_rot),
    init_scale(scale),
    rotateMatrix(RotateX(x_rot) * RotateY(y_rot) * RotateZ(z_rot)),
    m_scale(scale),
    position(position),
    modelMatrixFresh(false)
{}

/*
    Rotate and zoom methods implemented straightforwardly.
*/
void GLObjectState::rotateX(const GLfloat& rot){
    this->rotateMatrix = RotateX(rot) * this->rotateMatrix;
    this->modelMatrixFresh = false;
}

void GLObjectState::rotateY(const GLfloat& rot){
    this->rotateMatrix = RotateY(rot) * this->rotateMatrix;
    this->modelMatrixFresh = false;
}

void GLObjectState::rotateZ(const GLfloat& rot){
    this->rotateMatrix = RotateZ(rot) * this->rotateMatrix;
    this->modelMatrixFresh = false;
}

void GLObjectState::scale(const GLfloat& val){
    this->m_scale = this->m_scale * val;
    this->modelMatrixFresh = false;
}

void GLObjectState::scaleIn(const GLfloat& val){
    this->m_scale = this->m_scale * (1 + val);
    this->modelMatrixFresh = false;
}

void GLObjectState::scaleOut(const GLfloat& val){
    this->m_scale = this->m_scale * (1 - val);
    this->modelMatrixFresh = false;
}

void GLObjectState::move(const point3& move_val){
    this->position += move_val;
    this->modelMatrixFresh = false;
}

void GLObjectState::setPosition(const point3& newPosition){
    this->position = newPosition;
    this->modelMatrixFresh = false;
}

// reset method for reseting the object to its defualt.
void GLObjectState::reset(){
    this->resetRotation();
    this->m_scale = this->init_scale;
    this->modelMatrixFresh = false;
}

// resetRotation method resets only rotation, leaving scale constant.
void GLObjectState::resetRotation(){
    this->rotateMatrix = RotateX(this->init_x) * RotateY(this->init_y) * RotateZ(this->init_z);
    this->modelMatrixFresh = false;
}

void GLObjectState::updateModelMatrix(){
    this->modelMatrix = Translate(this->position) * Scale(this->m_scale, this->m_scale, this->m_scale) * this->rotateMatrix;
    this->modelMatrixFresh = true;
}

const point3& GLObjectState::getPosition() const {
    return this->position;
}

const mat4& GLObjectState::getModelMatrix(){
    if(!this->modelMatrixFresh)
        this->updateModelMatrix();

    return this->modelMatrix;
}

/*
    abstract GLObject class and its subclasses.
    Used for capsulating a objects properties,
    allocating memory from gpu and drawing.
*/

/* Static shader related members */
const char* GLObject::MODEL_MATRIX_NAME = "ModelMatrix";
const char* GLObject::VERTEX_POSITION_ATTRIB_NAME = "vPosition";
const char* GLObject::TEXTURE_COORDINATE_ATTRIB_NAME = "vTexCoord";
const char* GLObject::NORMAL_POSITION_ATTRIB_NAME = "vNormal";

GLObject::GLObject(GLuint program):
    program(program),
    modelMatrixLocation(glGetUniformLocation(program, MODEL_MATRIX_NAME)),
    positionAttrib(glGetAttribLocation(program, VERTEX_POSITION_ATTRIB_NAME)),
    texCoordAttrib(glGetAttribLocation(program, TEXTURE_COORDINATE_ATTRIB_NAME)),
    normalAttrib(glGetAttribLocation(program, NORMAL_POSITION_ATTRIB_NAME)),
    allocated(false)
{
    this->setMaterialLocations();
}

void GLObject::setMaterialLocations(){
    this->materialDiffuseActiveLocation = glGetUniformLocation(this->program, "material.diffuseActive");
    this->materialDiffuseLocation = glGetUniformLocation(this->program, "material.diffuse");
    this->materialSpecularActiveLocation = glGetUniformLocation(this->program, "material.specularActive");
    this->materialSpecularLocation = glGetUniformLocation(this->program, "material.specular");
    this->materialShininessLocation = glGetUniformLocation(this->program, "material.shininess");
}

void GLObject::loadMaterial(){
    glUseProgram(this->program);

    // Diffuse texture
    unsigned int diffuseTexId = this->materialProperties.getDiffuse();
    if(diffuseTexId > 0){
        Texture * diffuseTexture = TextureManager::getInstance()->getTexture(diffuseTexId);
        if(diffuseTexture != nullptr){
            glActiveTexture(GL_TEXTURE0);
            diffuseTexture->use();
            glUniform1i(this->materialDiffuseLocation, 0);
            glUniform1ui(this->materialDiffuseActiveLocation, true);
        } else {
            glUniform1ui(this->materialDiffuseActiveLocation, false);
        }
    } else {
        glUniform1ui(this->materialDiffuseActiveLocation, false);
    }

    // Specular texture
    unsigned int specularTexId = this->materialProperties.getSpecular();
    if(specularTexId > 0){
        Texture * specularTexture = TextureManager::getInstance()->getTexture(specularTexId);
        if(specularTexture != nullptr){
            glActiveTexture(GL_TEXTURE1);
            specularTexture->use();
            glUniform1i(this->materialSpecularLocation, 1);
            glUniform1ui(this->materialSpecularActiveLocation, true);
        } else {
            glUniform1ui(this->materialSpecularActiveLocation, false);
        }
    } else {
        glUniform1ui(this->materialSpecularActiveLocation, false);
    }

    glUniform1f(this->materialShininessLocation, this->materialProperties.getShininess());
    glUseProgram(0);

    const GLenum ErrorValue = glGetError();
    if (ErrorValue != GL_NO_ERROR)
    {
        fprintf(stderr, "%s\n", gluErrorString(ErrorValue));
        exit(EXIT_FAILURE);
    }
}


void GLObject::rotateX(const GLfloat& rot){
    this->objectState.rotateX(rot);
}

void GLObject::rotateY(const GLfloat& rot){
    this->objectState.rotateY(rot);
}

void GLObject::rotateZ(const GLfloat& rot){
    this->objectState.rotateZ(rot);
}


void GLObject::scale(const GLfloat& val){
    this->objectState.scale(val);
}


void GLObject::move(const point3& move_val){
    this->objectState.move(move_val);
}


void GLObject::setPosition(const point3& newPosition){
    this->objectState.setPosition(newPosition);
}


void GLObject::loadModelMatrix(const mat4& modelMatrix){
    glUseProgram(this->program);
    glUniformMatrix4fv(this->modelMatrixLocation, 1, GL_TRUE, modelMatrix);
    glUseProgram(0);
}


void GLObject::loadModelMatrix(){
    this->loadModelMatrix(this->objectState.getModelMatrix());
}

/*
void GLObject::unloadTexture(){
    glUseProgram(this->program);
    if(this->tex_id > 0){
        Texture * texture = TextureManager::getInstance()->getTexture(this->tex_id);
        if(texture != nullptr){
            texture->unUse();
        }
    }
    glUniform1ui(this->textureActiveLocation, false);
    glUseProgram(0);
}
*/


bool GLObject::isAllocated(){
    return this->allocated;
}

void GLObject::setDiffuseTexture(const unsigned int& newTexture){
    this->materialProperties.setDiffuse(newTexture);
}

void GLObject::setSpecularTexture(const unsigned int& newTexture){
    this->materialProperties.setSpecular(newTexture);
}

void GLObject::setMaterialProperties(const MaterialProperties& m_properties){
    this->materialProperties = m_properties;
}

void GLObject::setShininess(const GLfloat& shininess){
    this->materialProperties.setShininess(shininess);
}

const point3& GLObject::getPosition() const {
    return this->objectState.getPosition();
}

const mat4& GLObject::getModelMatrix(){
    return this->objectState.getModelMatrix();
}


/*
    A simple unit cube implementation, placed in the origin.
    Vertices and indices are defualt member variables defined in header file.
    Just implemeted in order to debug easier.
    Uses element array buffer to draw the cube.
*/
GLCube::GLCube(GLuint program): GLObject(program){
    this->vao_id = 0;
    this->vbo_ids[0] = 0;
    this->vbo_ids[1] = 0;
}


void GLCube::allocate(){
    glUseProgram(program);
    // Create an initilize vertex array object
    if(vao_id == 0)
        glGenVertexArrays(1, &vao_id);

    glBindVertexArray(vao_id);
    // Create and initialize a buffer object;
    // First index for vertex objects, second for index objects.
    if(vbo_ids[0] == 0 && vbo_ids[1] == 0)
        glGenBuffers(2, &vbo_ids[0]);

    size_t size_vertices = sizeof(vertices);
    size_t size_texCoords = sizeof(texCoords);
    size_t size_normals = sizeof(normals);

    glBindBuffer(GL_ARRAY_BUFFER, vbo_ids[0]);
    glBufferData(GL_ARRAY_BUFFER, size_vertices + size_texCoords + size_normals, NULL, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, size_vertices, vertices);
    glBufferSubData(GL_ARRAY_BUFFER, size_vertices, size_texCoords, texCoords);
    glBufferSubData(GL_ARRAY_BUFFER, size_vertices + size_texCoords, size_normals, normals);

    // set up vertex attributes;
    glEnableVertexAttribArray(this->positionAttrib);
    glVertexAttribPointer(this->positionAttrib, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

    glEnableVertexAttribArray(this->texCoordAttrib);
    glVertexAttribPointer(this->texCoordAttrib, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(size_vertices));

    glEnableVertexAttribArray(this->normalAttrib);
    glVertexAttribPointer(this->normalAttrib, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(size_vertices + size_texCoords));

    // set up index buffer object.
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_ids[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    glBindVertexArray(0);
    glUseProgram(0);

    this->allocated = true;
}


void GLCube::draw(){
    if(!this->isAllocated())
        this->allocate();

    this->loadModelMatrix();
    this->loadMaterial();

    glUseProgram(this->program);
    glBindVertexArray(this->vao_id);

    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, (GLvoid*)0);

    glBindVertexArray(0);
    glUseProgram(0);
}


/*
    Implementation of GLOffObject.
*/
GLOffObject::GLOffObject(GLuint program, string filename) : GLObject(program) {
    this->vao_id = 0;
    this->vbo_ids[0] = 0;
    this->vbo_ids[1] = 0;

    // initials in the case parsing file fails.
    this->numVertices = 0;
    this->vertices = NULL;

    this->numIndices = 0;
    this->indices = NULL;

    FILE* fp = fopen(filename.c_str(), "r");

    if ( fp != NULL )
    {   
        cout << "Reading from OFF file " << filename << endl;
        // Accessed the file, read and parse.
        fseek(fp, 0L, SEEK_END);
        long size = ftell(fp);

        fseek(fp, 0L, SEEK_SET);
        char* buf = new char[size + 1];
        fread(buf, 1, size, fp);

        buf[size] = '\0';
        fclose(fp);
        
        // Tokenize the buffered file.
        string str(buf);
        string delimiters(" \n\t\r");
        vector<string> tokenized;
        tokenized = tokenize(tokenized, str, delimiters);

        size_t num_tokens = tokenized.size();

        // Parse tokenized string.
        if (tokenized.size() > 3 && tokenized[0] == "OFF"){
            int num_vertices = stoi(tokenized[1]);

            vector<point4> * vertex_vector = new vector<point4>();

            int i;
            for(i=0; i < num_vertices; i++){
                GLfloat x_val = stof(tokenized[3*i+4]);
                GLfloat y_val = stof(tokenized[3*i+5]);
                GLfloat z_val = stof(tokenized[3*i+6]);
                vertex_vector->push_back(point4(x_val, y_val, z_val, 1));
            }

            vector<GLuint> * indices_vector = new vector<GLuint>();

            unsigned int offset = 4 + 3*num_vertices;

            while(offset < num_tokens){
                int num_vert_on_face = stoi(tokenized[offset]);
                int num_triangles = num_vert_on_face - 2;
                int face_indices[num_vert_on_face];
                
                for(i = 0; i < num_vert_on_face; i++)
                    face_indices[i] = stoi(tokenized[offset + i + 1]);
    
                // generate a triagles the face.
                for(i = 0; i < num_triangles; i++){
                    indices_vector->push_back(face_indices[0]);
                    indices_vector->push_back(face_indices[i + 1]);
                    indices_vector->push_back(face_indices[i + 2]);
                }

                offset += num_vert_on_face + 1;
            }

            this->numVertices = num_vertices;
            this->numIndices = indices_vector->size();
            this->vertices = vertex_vector->data();
            this->indices = indices_vector->data();
        }
        else
            cout << "Invalid file header." << endl;
    }
    else
        cout << "Can't read OFF file." << endl;
}

void GLOffObject::allocate(){
    if (numVertices > 0 && numIndices > 0){
        glUseProgram(program);

        // Create an initilize vertex array object
        if(vao_id == 0)
            glGenVertexArrays(1, &vao_id);
        glBindVertexArray(vao_id);

        // Create and initialize a buffer object;
        // First index for vertex objects, second for index objects.
        if((vbo_ids[0] == 0) && (vbo_ids[1] == 0))
            glGenBuffers(2, &vbo_ids[0]);

        glBindBuffer(GL_ARRAY_BUFFER, vbo_ids[0]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices[0]) * numVertices, vertices, GL_STATIC_DRAW);

        // set up vertex arrays;
        glEnableVertexAttribArray(this->positionAttrib);
        glVertexAttribPointer(this->positionAttrib, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

        // set up index buffer objects.
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_ids[1]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) * numIndices, indices, GL_STATIC_DRAW);

        glBindVertexArray(0);
        glUseProgram(0);
    }
    this->allocated = true;
}

void GLOffObject::draw(){
    if(!this->isAllocated())
        this->allocate();

    this->loadModelMatrix();
    glUseProgram(program);
    glBindVertexArray(vao_id);

    glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, (GLvoid*)0);

    glBindVertexArray(0);
    glUseProgram(0);
}


GLOffXObject::GLOffXObject(GLuint program, string filename) : GLObject(program) {
    this->vao_id = 0;
    this->vbo_ids[0] = 0;
    this->vbo_ids[1] = 0;

    // initials
    this->numVertices = 0;
    this->vertices = NULL;

    this->numNormals = 0;
    this->normals = NULL;

    this->numIndices = 0;
    this->indices = NULL;

    this->parseOffx(filename);
}

/* Parses offx and sets vertex attributes */
void GLOffXObject::parseOffx(string filename){
    FILE* fp = fopen(filename.c_str(), "r");

    if ( fp != NULL )
    {   
        // Accessed the file, read and parse.
        fseek(fp, 0L, SEEK_END);
        long size = ftell(fp);

        fseek(fp, 0L, SEEK_SET);
        char* buf = new char[size + 1];
        fread(buf, 1, size, fp);

        buf[size] = '\0';
        fclose(fp);
        
        // Tokenize the buffered file.
        string str(buf);
        string delimiters(" \n\t\r");
        vector<string> tokenized;
        tokenized = tokenize(tokenized, str, delimiters);

        size_t num_tokens = tokenized.size();

        // Parse tokenized string.
        if (tokenized.size() > 3 && tokenized[0] == "OFFX"){
            int num_vertices = stoi(tokenized[1]);
            unsigned int offset = 4;

            vector<point4> * vertex_vector = new vector<point4>();
            vector<point2> * tex_vector = new vector<point2>();
            vector<point3> * normal_vector = new vector<point3>();
            vector<GLuint> * indices_vector = new vector<GLuint>();

            int i;
            for(i=0; i < num_vertices; i++){
                GLfloat x_val = stof(tokenized[3*i+offset]);
                GLfloat y_val = stof(tokenized[3*i+offset+1]);
                GLfloat z_val = stof(tokenized[3*i+offset+2]);
                vertex_vector->push_back(point4(x_val, y_val, z_val, 1.0));
            }

            offset += 3*num_vertices;

            while(offset < num_tokens && tokenized[offset].compare("vt") != 0){
                int num_vert_on_face = stoi(tokenized[offset]);
                int num_triangles = num_vert_on_face - 2;
                int face_indices[num_vert_on_face];
                
                for(i = 0; i < num_vert_on_face; i++)
                    face_indices[i] = stoi(tokenized[offset + i + 1]);
    
                // generates triagles on the face.
                for(i = 0; i < num_triangles; i++){
                    indices_vector->push_back(face_indices[0]);
                    indices_vector->push_back(face_indices[i + 1]);
                    indices_vector->push_back(face_indices[i + 2]);
                }

                offset += num_vert_on_face + 1;
            }

            while(offset < num_tokens && tokenized[offset].compare("vt") == 0){
                GLfloat x_val = stof(tokenized[offset + 1]);
                GLfloat y_val = stof(tokenized[offset + 2]);
                tex_vector->push_back(point2(x_val, y_val));
                offset += 3;
            }

            while(offset < num_tokens && tokenized[offset].compare("vn") == 0){
                GLfloat x_val = stof(tokenized[offset + 1]);
                GLfloat y_val = stof(tokenized[offset + 2]);
                GLfloat z_val = stof(tokenized[offset + 3]);
                normal_vector->push_back(point3(x_val, y_val, z_val));
                offset += 4;
            }

            this->numVertices = num_vertices;
            this->numTexCoords = tex_vector->size();
            this->numNormals = normal_vector->size();
            this->numIndices = indices_vector->size();
            this->vertices = vertex_vector->data();
            this->texCoords = tex_vector->data();
            this->normals = normal_vector->data();
            this->indices = indices_vector->data();
        }
        else{
            cout << "Invalid OFFX file header." << endl;
            exit(2);
        }
    }
    else{
        cout << "Can't read OFFX file." << endl;
        exit(2);
    }
}


void GLOffXObject::allocate(){
    if (numVertices > 0 && numIndices > 0){
        glUseProgram(program);

        // Create an initilize vertex array object
        if(vao_id == 0)
            glGenVertexArrays(1, &vao_id);
        glBindVertexArray(vao_id);

        // Create and initialize a buffer object;
        // First index for vertex objects, second for index objects.
        if((vbo_ids[0] == 0) && (vbo_ids[1] == 0))
            glGenBuffers(2, &vbo_ids[0]);

        size_t size_vertices = sizeof(vertices[0]) * numVertices;
        size_t size_texCoords = sizeof(texCoords[0]) * numTexCoords;
        size_t size_normals = sizeof(normals[0]) * numNormals;

        glBindBuffer(GL_ARRAY_BUFFER, vbo_ids[0]);
        glBufferData(GL_ARRAY_BUFFER, size_vertices + size_texCoords + size_normals, NULL, GL_STATIC_DRAW);
        glBufferSubData(GL_ARRAY_BUFFER, 0, size_vertices, vertices);
        glBufferSubData(GL_ARRAY_BUFFER, size_vertices, size_texCoords, texCoords);
        glBufferSubData(GL_ARRAY_BUFFER, size_vertices + size_texCoords, size_normals, normals);

        // set up vertex attributes;
        glEnableVertexAttribArray(this->positionAttrib);
        glVertexAttribPointer(this->positionAttrib, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

        glEnableVertexAttribArray(this->texCoordAttrib);
        glVertexAttribPointer(this->texCoordAttrib, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(size_vertices));

        glEnableVertexAttribArray(this->normalAttrib);
        glVertexAttribPointer(this->normalAttrib, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(size_vertices + size_texCoords));

        // set up index buffer objects.
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_ids[1]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) * numIndices, indices, GL_STATIC_DRAW);

        glBindVertexArray(0);
        glUseProgram(0);
    }
    this->allocated = true;
}

void GLOffXObject::draw(){
    if(!this->isAllocated())
        this->allocate();

    this->loadModelMatrix();
    this->loadMaterial();

    glUseProgram(program);
    glBindVertexArray(vao_id);

    glDrawElements(GL_TRIANGLES, numIndices, GL_UNSIGNED_INT, (GLvoid*)0);

    glBindVertexArray(0);
    glUseProgram(0);
}

