#ifndef __TERRAIN_H
#define __TERRAIN_H

#include "obj.h"

typedef Angel::vec4  color4;
typedef Angel::vec4  point4;

/* Base terrain unit class */
class TerrainUnit : public GLObject {
    protected:
        GLuint vao_id;
        GLuint vbo_ids[2];
        GLfloat size;

    public:
        explicit TerrainUnit(GLuint program, const GLfloat& size = 1.0);
        virtual void draw()=0;

        GLfloat getSize() const;
};

/* A simple terrain unit class, basically a square */
class SimpleTerrainUnit : public TerrainUnit {
    protected:
        GLfloat textureSize;

    public:
        explicit SimpleTerrainUnit(GLuint program, const GLfloat& size = 1.0, const GLfloat& textureSize = 1.0);
        virtual void allocate();
        virtual void draw();
};

/* Base terrain class, haven't decided to make a shared interface yet. */
class Terrain : public Drawable {
    public:
        explicit Terrain(): Drawable(){};
};

/* A simple terrain class, a rectangular repetition of terrains */
class SimpleTerrain : public Terrain {
    protected:
        TerrainUnit * terrainUnit;

        unsigned int x_num; // number terrain units in x direction
        unsigned int y_num; // number terrain units in y direction

        point3 center; // center of the terrain
        
    public:
        explicit SimpleTerrain(TerrainUnit * terrainUnit, const unsigned int& x_num = 1, const unsigned int& y_num = 1, const point3& center = point3(0.0, 0.0, 0.0));
        virtual void draw();


};

#endif