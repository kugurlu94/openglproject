#ifndef __LIGHT_H
#define __LIGHT_H

#include <Angel.h>

typedef Angel::vec4 color4;
typedef Angel::vec4 point4;
typedef Angel::vec3 point3;

class LightProperties {
    protected:
        /* Default light components */
        static const color4 DEFAULT_AMBIENT;
        static const color4 DEFAULT_DIFFUSE;
        static const color4 DEFAULT_SPECULAR;

        /* Light components members */
        color4 ambient;
        color4 diffuse;
        color4 specular;

    public:
        /* Explicit constructor */
        explicit LightProperties(
            const color4& ambient_c = DEFAULT_AMBIENT,
            const color4& diffuse_c = DEFAULT_DIFFUSE,
            const color4& specular_c = DEFAULT_SPECULAR
        );

        /* Getters */
        color4 getAmbient() const;
        color4 getDiffuse() const;
        color4 getSpecular() const;

        /* Setters */
        void setAmbient(const color4& ambient);
        void setDiffuse(const color4& diffuse);
        void setSpecular(const color4& specular);

};


class AttenuationProperties {
    protected:
        static const GLfloat DEFAULT_CONSTANT;
        static const GLfloat DEFAULT_LINEAR;
        static const GLfloat DEFAULT_QUADRATIC;

        GLfloat constant;
        GLfloat linear;
        GLfloat quadratic;
    public:
        explicit AttenuationProperties(
            const GLfloat& constant = DEFAULT_CONSTANT,
            const GLfloat& linear = DEFAULT_LINEAR,
            const GLfloat& quadratic = DEFAULT_QUADRATIC);

        /* Getters */
        GLfloat getConstant() const;
        GLfloat getLinear() const;
        GLfloat getQuadratic() const;

        /* Setters */
        void setConstant(const GLfloat& constant);
        void setLinear(const GLfloat& linear);
        void setQuadratic(const GLfloat& quadratic);
};

// Enumeration for differentiating source types.
enum LightSourceType { POINT_SOURCE, DIRECTIONAL_SOURCE, SPOT_SOURCE };

// Abstract Base LightSource class
class LightSource {
    protected:
        static const LightProperties& DEFAULT_LIGHT_PROPERTIES;

        LightProperties properties;

    public:
        explicit LightSource(const LightProperties& l_properties = DEFAULT_LIGHT_PROPERTIES);

        /* getters */
        const LightProperties& getProperties() const;

        /* setters */
        void setProperties(const LightProperties& l_properties);

        virtual LightSourceType getType() const =0; // used for getting source type.
};


class PointLightSource : public LightSource {
    protected:
        static const AttenuationProperties& DEFAULT_ATTENUATION;
        static const point3& DEFAULT_POSITION;

        point3 position;
        AttenuationProperties attenuationProperties;

    public:
        explicit PointLightSource(
            const point3& l_position = DEFAULT_POSITION,
            const LightProperties& l_properties = DEFAULT_LIGHT_PROPERTIES,
            const AttenuationProperties& attenuationProperties = DEFAULT_ATTENUATION);

        /* getters */
        const AttenuationProperties& getAttenuationProperties() const;
        const point3& getPosition() const;

        /* setters */
        void setAttenuationProperties(const AttenuationProperties& attenuation);
        void setPosition(const point3& l_position);

        virtual LightSourceType getType() const;
};


class DirectionalLightSource : public LightSource {
    protected:
        static const vec3& DEFAULT_DIRECTION;

        vec3 direction;

    public:
        explicit DirectionalLightSource(
            const vec3& direction = DEFAULT_DIRECTION,
            const LightProperties& l_properties = DEFAULT_LIGHT_PROPERTIES);

        /* getters */
        const vec3& getDirection() const;

        /* setters */
        void setDirection(const vec3& direction);

        virtual LightSourceType getType() const;
};


class SpotLightSource : public PointLightSource {
    protected:
        static const vec3& DEFAULT_DIRECTION;
        static const GLfloat& DEFAULT_CUTOFF_ANGLE;
        static const GLfloat& DEFAULT_OUTER_CUTOFF_ANGLE;

        vec3 direction;
        GLfloat cutOffAngle;
        GLfloat outerCutOffAngle;

    public:
        explicit SpotLightSource(
            const point3& l_position = DEFAULT_POSITION,
            const vec3& direction = DEFAULT_DIRECTION,
            const GLfloat& cutOffAngle = DEFAULT_CUTOFF_ANGLE,
            const GLfloat& outerCutOffAngle = DEFAULT_OUTER_CUTOFF_ANGLE,
            const LightProperties& l_properties = DEFAULT_LIGHT_PROPERTIES,
            const AttenuationProperties& attenuationProperties = DEFAULT_ATTENUATION);

        /* getters */
        const vec3& getDirection() const;
        const GLfloat& getCutOffAngle() const;
        const GLfloat& getOuterCufOffAngle() const;

        /* setters */
        void setDirection(const vec3& direction);
        void setCutOffAngle(const GLfloat& cutOff);
        void setOuterCutOffAngle(const GLfloat& outerCutOff);

        virtual LightSourceType getType() const;

};


class LightingManager {
    protected:
        const static int MAX_POINT_SOURCES = 25;
        const static int MAX_DIRECTIONAL_SOURCES = 5;
        const static int MAX_SPOT_SOURCES = 5;

        /* program related */
        GLuint program;

        /* point source uniform locations */
        GLuint point_enabledLocation;
        GLuint point_constantLocation;
        GLuint point_linearLocation;
        GLuint point_quadraticLocation;
        GLuint point_ambientLocation;
        GLuint point_diffuseLocation;
        GLuint point_specularLocation;
        GLuint point_positionLocation;
        GLuint point_locationOffset;

        /* directional source uniform locations */
        GLuint dir_enabledLocation;
        GLuint dir_ambientLocation;
        GLuint dir_diffuseLocation;
        GLuint dir_specularLocation;
        GLuint dir_directionLocation;
        GLuint dir_locationOffset;

        /* spot source uniform locations */
        GLuint spot_enabledLocation;
        GLuint spot_constantLocation;
        GLuint spot_linearLocation;
        GLuint spot_quadraticLocation;
        GLuint spot_ambientLocation;
        GLuint spot_diffuseLocation;
        GLuint spot_specularLocation;
        GLuint spot_positionLocation;
        GLuint spot_directionLocation;
        GLuint spot_cutOffCosineLocation;
        GLuint spot_outerCutOffCosineLocation;
        GLuint spot_locationOffset;

        GLuint lastPointSourceLocation;
        GLuint lastDirectionalSourceLocation;
        GLuint lastSpotSourceLocation;

        /* Containers */
        PointLightSource pointSources[MAX_POINT_SOURCES];
        DirectionalLightSource directionalSources[MAX_DIRECTIONAL_SOURCES];
        SpotLightSource spotSources[MAX_SPOT_SOURCES];

        bool pointStatuses[MAX_POINT_SOURCES];
        bool directionalStatuses[MAX_DIRECTIONAL_SOURCES];
        bool spotStatuses[MAX_SPOT_SOURCES];

        /* Keeping eye on active sources */
        int lastPointSource = -1;
        int lastDirectionalSource = -1;
        int lastSpotSource = -1;

        void allocateLightSource(const GLuint& program);

    public:
        explicit LightingManager(const GLuint& program);

        /* add a light source */
        GLuint addLightSource(const LightSource& l_source);

        /* enable a light source */
        void enableLightSource(const GLuint& source_id);

        /* disable a light source */
        void disableLightSource(const GLuint& source_id);

        /* get light source enabled info*/
        bool lightEnabled(const GLuint& source_id);

        /* set light properties */
        void setProperties(const GLuint& source_id, const LightProperties& l_properties);

        /* change a light sources position (for point source and spot source) */
        void setPosition(const GLuint& source_id, const point3& position);

        /* change a light sources direction */
        void setDirection(const GLuint& source_id, const vec3& direction);
};

#endif