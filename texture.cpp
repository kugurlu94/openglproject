#include "texture.h"

Texture::Texture(GLsizei width, GLsizei height, const GLubyte * image):
    width(width), height(height), tex_id(0), image(image), allocated(false){}

Texture::~Texture(){
    if(this->image != NULL)
        delete this->image;
}

void Texture::allocate(){
    // Load textures.
    if(!this->isAllocated())
        glGenTextures(1, &(this->tex_id));

    glBindTexture(GL_TEXTURE_2D, this->tex_id);
    glTexImage2D(
        GL_TEXTURE_2D, 0, GL_RGB,
        this->width,
        this->height,
        0, GL_RGB, GL_UNSIGNED_BYTE,
        this->image);
    glGenerateMipmap(GL_TEXTURE_2D);

    this->allocated = true;
    this->updateProperties();
    this->unUse();
}

void Texture::deallocate(){
    if(this->isAllocated())
        glDeleteTextures(1, &(this->tex_id));

    this->tex_id = 0;
    this->allocated = false;
}

bool Texture::isAllocated(){
    return this->allocated;
}

void Texture::use(){
    if(!this->isAllocated()) // lazy allocation
        this->allocate();

    glBindTexture(GL_TEXTURE_2D, this->tex_id);
}

void Texture::unUse(){
    glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::updateProperties(){
    this->use();
    if(this->tex_id > 0){
        glTextureParameteri(this->tex_id, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTextureParameteri(this->tex_id, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTextureParameteri(this->tex_id, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTextureParameteri(this->tex_id, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
    }
}

/* static factory methods */

/* Parse the ppm file and set image properties. */
Texture * Texture::fromPPM(string imageFilename){
    /* Read image file */
    FILE* fp = fopen(imageFilename.c_str(), "r");

    if ( fp != NULL ){
        fseek(fp, 0L, SEEK_END);
        long size = ftell(fp);

        fseek(fp, 0L, SEEK_SET);
        char* buf = new char[size + 1];
        fread(buf, 1, size, fp);

        buf[size] = '\0';
        fclose(fp);
        
        // Tokenize the buffered file.
        string str(buf);
        string delimiters("\n\r");
        vector<string> tokenized;
        tokenized = tokenize(tokenized, str, delimiters);

        size_t num_tokens = tokenized.size();

        // Parse tokenized string.
        if (tokenized.size() > 1 && tokenized[0] == "P3"){
            unsigned int offset = 1;
            while(offset < num_tokens && tokenized[offset][0] == '#'){
                // skip comments
                offset++;
            }

            string headerDeliminers = " ";
            vector<string> header;
            tokenize(header, tokenized[offset], headerDeliminers);
            int textureRowLength = stoi(header[0]);
            int textureColumnLength = stoi(header[1]);
            offset++;

            size_t imageSize = textureRowLength * textureColumnLength;
            GLubyte * textureImage = (GLubyte *) malloc(3 * imageSize * sizeof(GLubyte));

            int rowCounter = 0;
            int columnCounter = 0;
            string lineDelimiters = " ";
            while(offset < num_tokens){
                vector<string> tokens;
                tokenize(tokens, tokenized[offset], lineDelimiters);
                int numTokens = tokens.size();
                int i;
                for(i=0; 3*i + 2 < numTokens; i++){
                    int currentTexel = ((textureRowLength - rowCounter - 1) * textureColumnLength + columnCounter);
                    textureImage[3*currentTexel]     = (GLubyte) stoi(tokens[3*i]);
                    textureImage[3*currentTexel + 1] = (GLubyte) stoi(tokens[3*i + 1]);
                    textureImage[3*currentTexel + 2] = (GLubyte) stoi(tokens[3*i + 2]);
                    columnCounter++;
                    if(columnCounter == textureColumnLength){
                        columnCounter = 0;
                        rowCounter++;
                    }
                }
                offset++;
            }
            return new Texture(textureRowLength, textureColumnLength, textureImage);
        }
        else{
            cout << "Invalid PPM file header." << endl;
            exit(2);
        }
    }
    else{
        cout << "Can't read PPM image file." << endl;
        exit(2);
    }
}

/* Parse the image file to into its pixels and create a new Texture.*/
Texture * Texture::fromImageFile(string imageFilename){
    GLsizei width, height;
    unsigned char* image = SOIL_load_image(imageFilename.c_str(), &width, &height, 0, SOIL_LOAD_RGB);

    return new Texture(width, height, image);
};


TextureManager * TextureManager::instance = nullptr;

unsigned int TextureManager::newTexture(GLsizei width, GLsizei height, const GLubyte *image){
    Texture * newTex = new Texture(width, height, image);
    this->textures.push_back(newTex);
    return this->textures.size();
}

unsigned int TextureManager::fromPPM(string imageFilename){
    Texture * newTex = Texture::fromPPM(imageFilename);
    this->textures.push_back(newTex);
    return this->textures.size();
}

unsigned int TextureManager::fromImageFile(string imageFilename){
    Texture * newTex = Texture::fromImageFile(imageFilename);
    this->textures.push_back(newTex);
    return this->textures.size();
}

Texture * TextureManager::getTexture(const unsigned int tex_id) const {
    if(tex_id > 0 && tex_id <= this->textures.size())
        return this->textures[tex_id - 1];
    else
        return nullptr;
}

void TextureManager::removeAll(){
    unsigned int i;
    size_t size_i = this->textures.size();
    Texture * currentTex = nullptr;

    for(i=0; i < size_i; i++){
        currentTex = this->textures[i];
        if(currentTex != nullptr){
            currentTex->deallocate();
            delete currentTex;
        }
    }

    this->textures.clear();
};

TextureManager * TextureManager::getInstance(){
    if(instance == nullptr)
        instance = new TextureManager();

    return instance;
}
