#ifndef __UTILS_H
#define __UTILS_H

#include <vector>
#include <string>

using namespace std;

vector<string>& tokenize(vector<string>& result, string& str, string& delimiters);

#endif