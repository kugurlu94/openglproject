#include "utils.h"

// Utility string tokenizer method for tokenizing the offfile data.
vector<string>& tokenize(vector<string>& result, string& str, string& delimiters){
    // Retuns a vector of strings, tokenized from str using chars in the delimiters.
    result.clear();
    size_t current;
    size_t next = -1;
    do
    {
        current = str.find_first_not_of(delimiters, next + 1);
        if (current == string::npos)
            break;
        next = str.find_first_of(delimiters, current);
        result.push_back(str.substr(current, next - current));
    } while (next != string::npos);

    return result;
}